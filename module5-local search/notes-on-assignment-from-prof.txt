Class,

I've gotten a few emails about the assignment and I want to share the collective wisdom.

1. Individuals are definitely a full solution for 10 variables. In the binary case, it's a List of 100 1's or 0's that you initialize at random. For the real valued case, it's a List of 10 "real" numbers that are initialized uniformly at random.

2. The binary range I have excludes 1024. That's ok. If the exclusion of 1024 mattered in the real world, you should pick a larger range and a bigger encoding. It's not really a good practice to have potential solutions lie at the extremes of your boundaries. If you start noticing this in your solutions (that the GA finds solutions near the boundaries), stop and increase the size of your boundaries/change your encoding.

3. Premature convergence is always a problem in GAs both from an algorithmic point of view and, sometimes, a programming point of view. From the latter, make sure you don't have any subtle bugs like modifying individuals in place instead of copying them (or using operations that make copies) or not correctly generating a new population. Make sure your crossover and mutation operators are doing what you think. This is the beauty of a functional approach. You should have individual functions that can be tested. Put in two parents, see what the children look like and see if the parents are unmodified.

(If you really, really want to be strict about it the actual crossover function should not pick the crossover point but take an index as an argument as well, this way you can make sure the RNG Gods are not messing with you!)

From the algorithmic point of view, try different parameter values, especially for mutation.

4. Mutation can be done in a number of ways (in either algorithm).  You can 1. test on a gene by gene basis and then mutate as appropriate or 2. you can test on an individual basis, then randomly pick the gene to be mutated, then mutate as appropriate. The overall rate, however, should equal out to be the same.

If you want an overall rate of 5% (say), though, this won't be the same rate for version #1 for both binary and the real valued encodings because for the binary encoding there are 100 tests per individual and for the real valued encoding there are only 10 tests per individual.


What I generally find is that students do not run the GA for nearly long enough. If you're not getting the correct result and the code is correct, you're not running it for long enough or with enough individuals or both.

Here's output for someone who ran their algorithm for N generations:

Executing Binary GA
Best individual:
genotype: 1000110010011111111110010000001000110010100010111110010000001000110010100011001010001100101000101111
phenotype: [0.5, -0.01, 0.64, 0.5, 0.47, 0.64, 0.5, 0.5, 0.5, 0.47]
fitness: 0.7685804319422028
f value: 0.30110000000000003

Their results almost always have that weird "-0.01" value, often in the same place and sometimes in multiple places.

Here's a different submission that runs for many more than N generations:

Final Result 1001 
Best individual:   
Genotype: 1000101011100011011010010001001000101100100010100110001010111000101110100010110110001100111000110010   
Phenotype: [0.43, 0.54, 0.68, 0.44, 0.41, 0.43, 0.46, 0.45, 0.51, 0.5]   
Fitness 0.9436633009342266   
Func value: 0.059700000000000114

This implementation consistently finds values very near the optimal ones.

(As you may notice by now, finding "many more than N" is part of the assignment or I would just tell you want to do! :D )

Please also note that both the genotype and phenotype are printed out.

Cheers,
Stephyn