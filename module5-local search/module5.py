import random
import sys
from copy import deepcopy
from typing import List, Tuple


def sphere(shift, xs):
    return sum([(x - shift) ** 2 for x in xs])


def convert_binary_individual_to_real(individual: List[int]) -> float:
    """ Returns the float value of a binary encoded individual (-5.11 to 5.11)

    Args:
        individual: Binary encoded individual

    Returns:
        Float value of the binary encoded individual

    """

    return normalize_binary_encoding(sum(individual[i] * 512 / 2 ** i for i in range(0, 10)))


def normalize_binary_encoding(value: int) -> float:
    """ Converts the binary value (0 to 1023) to a float value (-5.12 to 5.11)

    Args:
        value: An integer value from 0 to 1023

    Returns:
        Float value from -5.12 to 5.11

    """

    return (value - 512) / 100


def generate_random_binary_individual() -> List[int]:
    """ Returns a list that contains 10 randomly initialized binary individuals in a single list.

    """

    return [random.randint(0, 1) for i in range(100)]


def convert_binary_genotype_to_phenotype(genotype: List[int]) -> List[float]:
    """ Converts a binary phenotype to a genotype.

    Args:
        genotype: Genotype, containing 1's and 0's

    Returns:
        List containing ten floats representing the phenotype.

    """

    return [convert_binary_individual_to_real(genotype[i * 10:(i + 1) * 10]) for i in range(10)]


def generate_random_real_individual() -> List[float]:
    """ Generates a list that contains 10 randomly initialized real individuals in a single list.

    Returns:
        List containing 10 random floats between -5.12 and 5.12.

    """
    return [random.uniform(-5.12, 5.12) for i in range(10)]


def evaluate(parameters, best_genotype: List[int], best_phenotype: List[float], best_fitness: float):
    """ Evaluates the fitness of the given population. If it is better, the given population is set as the "best".

    Args:
        parameters: Dict of common parameters for the population, including genotype & phenotype
        best_genotype: The best genotype observed thus far for the given population
        best_phenotype: The best phenotype observed thus far for the given population
        best_fitness: The best fitness observed thus far for the given population

    Returns:
        Tuple containing the best genotype, best phenotype, and best fitness value.

    """
    f_value = parameters["f"](parameters["phenotype"])
    if parameters["minimization"]:
        fitness = 1 / (1 + f_value)
    else:
        fitness = f_value

    if fitness > best_fitness:
        best_fitness = fitness
        best_genotype = parameters["population"]
        best_phenotype = parameters["phenotype"]

    return best_genotype, best_phenotype, best_fitness


def pick_parents(parameters):
    """ Function to choose 5 sets of parents in a random fashion, but weighted according to each parents' fitness.

    The fitness of each parent in the current generation is assessed by looking at each phenotype's proportion of
    1 / (1 + f). Then, two parents are chosen at random, with replacement, according to the probabilities of their
    weights of the total fitness.

    Args:
        parameters: Dict of common parameters for the population, including genotype & phenotype

    Returns:
        Tuple containing two parents genotypes.

    """
    weighted_phenotypes = []
    fitness_proportions = []
    n = len(parameters["phenotype"])
    for i in range(n):
        if parameters["minimization"]:
            weighted_phenotypes.append(1 / (1 + parameters["f"]([(parameters["phenotype"][i])])))
        else:
            weighted_phenotypes.append(parameters["f"]([(parameters["phenotype"][i])]))

    sum_of_weighted_phenotypes = sum(weighted_phenotypes)

    for i in range(n):
        fitness_proportions.append(weighted_phenotypes[i] / sum_of_weighted_phenotypes)

    # choose a parent randomly, but according to the parents' proportion of fitness
    parent1_index = random.choices(range(n), fitness_proportions)[0]
    parent2_index = random.choices(range(n), fitness_proportions)[0]

    # assign parents
    parent1 = parameters["get_individual_from_population"](parent1_index, parameters["population"])
    parent2 = parameters["get_individual_from_population"](parent2_index, parameters["population"])
    return parent1, parent2


def mutate_binary_child(child: List[int]) -> List[int]:
    """ Mutates a binary child by choosing an index at random and negating its value.

    Args:
        child: The child to mutate.

    Returns:
        The mutated binary child.

    """
    random_index = random.randint(0, 9)
    child[random_index] = int(not child[random_index])
    return child


def crossover_binary_children(child1: List[int], child2: List[int]):
    """ Crossover function for the binary population that swaps the bit from the same index for two children.

    Args:
        child1: a genetic algorithm child
        child2: another genetic algorithm child

    Returns:
        A tuple containing both children with one bit from the same index swapped from each.

    """
    random_index = random.randint(0, 9)
    child1_bit = child1[random_index]
    child2_bit = child2[random_index]
    child1[random_index] = child2_bit
    child2[random_index] = child1_bit
    return child1, child2


def mutate_real_child(child: float):
    """ Mutates a floating point child by generating Gaussian noise and applying it to the current child.

    If the value that results happens to be outside of the bounds of -5.12 <= x <= 5.12, then keep generating
    possibilities until we find out that is in bounds.

    Args:
        child: A real child to whom Gaussian noise will be applied.

    Returns:
        A new child that has had Gaussian noise applied to it.
    """

    # apply "Gaussian noise"
    new_child = child + random.gauss(0, 1)

    # make sure that the value that we generated is within the confines of our problem space
    while not (-5.12 <= new_child <= 5.12):
        new_child = child + random.gauss(0, 1)
    return new_child


def reproduce(parameters, parents: Tuple[float, float]) -> Tuple[float, float]:
    """ Function that takes two parents, applies crossover and mutation and returns two children.

    The probability of crossover is 95%.
    The probability of mutation is 5% for each child.

    Args:
        parameters,
        parent1: real parent #1
        parent2: real parent #2

    Returns:
        A tuple containing the two children.

    """

    child1: float = deepcopy(parents[0])
    child2: float = deepcopy(parents[1])

    if random.random() < parameters["prob_of_crossover"]:
        # crossover the two children
        child1, child2 = parameters["crossover_function"]((child2, child1))
    if random.random() < parameters["prob_of_mutation"]:
        # mutate child1
        child1 = parameters["mutation_function"](child1)
    if random.random() < parameters["prob_of_mutation"]:
        # mutate child2
        child2 = parameters["mutation_function"](child2)
    return child1, child2


def execute_ga(parameters) -> Tuple[List[float], List[float], float]:
    """ Function to run the shifting spheres genetic algorithm on a number population.

    The function runs 200,000 generations on a real valued population where the genotype and phenotype are identical.

    The evaluation function determines the fitness of the current generation and sets it to the best if it is greater
    than the previous best value.

    Then, ten new individuals are chosen by randomly reproducing the current individuals according to their proportion
    of the total fitness thus far.

    When the randomly chosen individuals reproduce, they crossover and mutate according to their defined probabilities.

    At the end of each generation, the new generation is set and the process continues until all the generations
    execute.

    Finally, the best individual is returned.


    Args:
        parameters: Dict of common parameters for the population, including genotype & phenotype

    Returns:
        Tuple containing best individual's genotype, phenotype, and fitness.
    """
    generations: int = 0
    limit: int = 200_000
    best_genotype = []
    best_phenotype = []
    best_fitness = -sys.maxsize
    while generations < limit:

        best_genotype, best_phenotype, best_fitness = evaluate(parameters, best_genotype, best_phenotype,
                                                               best_fitness)
        next_population = []
        for n in range(0, int(len(parameters["phenotype"]) / 2)):
            parent1, parent2 = pick_parents(parameters)
            child1, child2 = reproduce(parameters, (parent1, parent2))
            parameters["add_to_new_population"](next_population, child1)
            parameters["add_to_new_population"](next_population, child2)
        parameters["population"] = next_population
        parameters["phenotype"] = parameters["convert_geno_to_pheno"](parameters["population"])
        generations += 1

        if debug and generations % (limit / 20) == 0:
            print(f'Generation #{generations}')
            print('Best Individual:')
            print(f'\tGenotype:\t{best_genotype}')
            print(f'\tPhenotype:\t{best_phenotype}')
            print(f'\tFitness:\t{best_fitness}')
            print(f'\tFunction value:\t{parameters["f"](best_genotype)}')

    return best_genotype, best_phenotype, best_fitness


if __name__ == "__main__":
    # use this debug flag if necessary; otherwise, the program should print out only
    # what is requested.
    debug = len(sys.argv) > 1 and sys.argv[1].lower() == 'debug'

    # Traditional GA
    # TODO remove
    debug = True
    binary_population = generate_random_binary_individual()
    parameters = {  # should include "at least" genome (float value) and fitness score
            "f": lambda xs: sphere(0.5, xs),  # lambda is expecting genome (list of floats)
            "minimization": True,
            "population": binary_population,
            "phenotype": convert_binary_genotype_to_phenotype(binary_population),
            "prob_of_crossover": 0.95,
            "prob_of_mutation": 0.24,
            "crossover_function": lambda children: crossover_binary_children(children[1], children[0]),
            "mutation_function": lambda child: mutate_binary_child(child),
            "add_to_new_population": lambda population, child: population.extend(child),
            "convert_geno_to_pheno": lambda genotype: convert_binary_genotype_to_phenotype(genotype),
            "get_individual_from_population": lambda index, population: population[index * 10:(index + 1) * 10]
    }

    print('Executing Binary GA')
    best_binary_genotype, best_binary_phenotype, best_binary_fitness = execute_ga(parameters)
    print('Final Result')
    print('Best Individual:')
    print(f'\tGenotype:\t{best_binary_genotype}')
    print(f'\tPhenotype:\t{best_binary_phenotype}')
    print(f'\tFitness:\t{best_binary_fitness}')
    print(f'\tFunction value:\t{parameters["f"](best_binary_phenotype)}')
    print(' ')

    # Real Valued GA
    real_population = generate_random_real_individual()
    parameters = {
            "f": lambda xs: sphere(0.5, xs),
            "minimization": True,
            "population": real_population,
            "phenotype": real_population,
            "prob_of_crossover": 0.90,
            "prob_of_mutation": 0.05,
            "crossover_function": lambda children: (children[1], children[0]),
            "mutation_function": lambda child: mutate_real_child(child),
            "add_to_new_population": lambda population, child: population.append(child),
            "convert_geno_to_pheno": lambda genotype: genotype,
            "get_individual_from_population": lambda index, population: population[index]
    }
    print('Executing Real GA')
    best_real_genotype, best_real_phenotype, best_real_fitness = execute_ga(parameters)
    print('Final Result')
    print('Best Individual:')
    print(f'\tGenotype:\t{best_real_genotype}')
    print(f'\tPhenotype:\t{best_real_phenotype}')
    print(f'\tFitness:\t{best_real_fitness}')
    print(f'\tFunction value:\t{parameters["f"](best_real_phenotype)}')
    print('')
