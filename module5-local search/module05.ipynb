{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Module 5 - Programming Assignment\n",
    "\n",
    "## Directions\n",
    "\n",
    "There are general instructions on Blackboard and in the Syllabus for Programming Assignments. This Notebook also has instructions specific to this assignment. Read all the instructions carefully and make sure you understand them. Please ask questions on the discussion boards or email me at `EN605.445@gmail.com` if you do not understand something.\n",
    "\n",
    "<div style=\"background: mistyrose; color: firebrick; border: 2px solid darkred; padding: 5px; margin: 10px;\">\n",
    "Please follow the directions and make sure you provide the requested output. \n",
    "Failure to do so may result in a lower grade, possibly 0 points, even if the code is correct.</div>\n",
    "\n",
    "You must submit your assignment as `<jhed_id>.ipynb`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "from IPython.core.display import *\n",
    "from io import StringIO\n",
    "import random\n",
    "import sys\n",
    "from copy import deepcopy\n",
    "from typing import List, Tuple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Local Search - Genetic Algorithm\n",
    "\n",
    "For this assignment we're going to use the Genetic Algorithm to find the solution to a shifted Sphere Function in 10 dimensions, $x$, where the range of $x_i$ in each dimension is (-5.12 to 5.12). Here a \"solution\" means the vector $x$ that minimizes the function. The Sphere Function is:\n",
    "\n",
    "$$f(x)=\\sum x^2_i$$\n",
    "\n",
    "We are going to shift it over 0.5 in every dimension:\n",
    "\n",
    "$$f(x) = \\sum (x_i - 0.5)^2$$\n",
    "\n",
    "where $n = 10$.\n",
    "\n",
    "As this *is* a minimization problem you'll need to use the trick described in the lecture to turn the shifted Sphere Function into an appropriate fitness function (which is always looking for a *maximum* value)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You are going to solve the problem two different ways. First, using the traditional (or \"Canonical\") Genetic Algorithm and then using a Real Valued GA that uses a List of floating values.\n",
    "\n",
    "There are a few points to remember:\n",
    "\n",
    "1. Every individual in the population represents a complete solution. This means that if our problem requires 2 variables, each individual encodes values for those two variables. If it requires values for 10 variables, each individual encodes 10 values...100 variables, 100 values.\n",
    "2. You are often able to trade off generations and population size but, as mentioned in the lectures, population sizes are usually on the order of 100s, not 10s.\n",
    "3. The encoding is the *genotype* and the decode values are the *phenotype* in the same way that genes that influence eye color are the genotype and blue eyes are the phenotype.\n",
    "\n",
    "## Binary GA\n",
    "\n",
    "The Binary GA encodes numeric values as a single binary string (you don't have to represent them literally as strings but they are general lists or strings of only 0 or 1).\n",
    "\n",
    "There are many different ways to affect this encoding. For this assignment, the easiest is probably to use a 10 bit binary encoding for each $x_i$. This gives each $x_i$ a potential value of 0 to 1024 which can be mapped to (-5.12, 5.12) by subtracting 512 and dividing by 100. This is not exact but good enough for our purposes; if you think that the optimal solution lies in a different range, you would need a different encoding.\n",
    "\n",
    "All the GA operators will be as described in the lecture.\n",
    "\n",
    "**Important**\n",
    "\n",
    "Please remember that there is a difference between the *genotype* and the *phenotype* for the Binary GA. The GA operates on the *genotype* (the encoding) and does not respect the boundaries of the phenotype (the decoding)...it's just one long string of 0's and 1's just your genes are just long strings of G, A, C, Ts. So, for example, do **not** use a List of Lists to represent an individual. It should be a *single* List of 10 x 10 or 100 bits. In general, crossover and mutation have no idea what those bits represent...just as in the real world, if a section of your genes encodes for lactose tolerance or intolerance, you may get portions of that section from your mother and portions from your father. You don't necessarily get the whole section from either your mother or father."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Real Valued GA\n",
    "\n",
    "For the real valued GA, you can represent each $x_i$ as a float in the range (-5.12, 5.12) but you will need to create a new mutation operator that applies gaussian noise. Python's random number generator for the normal distribution is called `gauss` and is found in the random module:\n",
    "\n",
    "```\n",
    "from random import gauss, random\n",
    "```\n",
    "\n",
    "You may need to experiment a bit with the standard deviation of the noise but the mean will be 0.0. The interesting thing here is that for the *Real valued* GA, the genotype and phenotype aren't different. This quite novel back when the algorithm was developed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## GA\n",
    "\n",
    "The Genetic Algorithm itself will have the same basic structure in each case: create a population, evaluate it, select parents, apply crossover and mutation, repeat until the number of desired generations have been generated. The easiest way to accomplish this in \"Functional\" Python would be to use Higher Order Functions.\n",
    "\n",
    "In order for me to determine that your algorithm is doing the right thing, please print out:\n",
    "\n",
    "1. generation #\n",
    "2. best individual *of the generation*\n",
    "    1. genotype\n",
    "    2. phenotype\n",
    "    3. fitness\n",
    "    4. function value\n",
    "\n",
    "every 25 generations if the debug flag is true and have your function return the best individual seen over the course of the algorithm (and print that out as well).\n",
    "\n",
    "The GA has a lot of parameters: mutation rate, crossover rate, population size, dimensions (given for this problem), number of generations.  You can put all of those and your fitness function in a `Dict` in which case you need to implement:\n",
    "\n",
    "```python\n",
    "# def binary_ga(parameters):\n",
    "#   pass\n",
    "```\n",
    "\n",
    "and\n",
    "\n",
    "```python\n",
    "# def real_ga(parameters):\n",
    "#   pass\n",
    "```\n",
    "\n",
    "Remember that you need to transform the sphere function into a legit fitness function. Since you also need the sphere function, I would suggest that your parameters Dict includes something like:\n",
    "\n",
    "```python\n",
    "# parameters = {\n",
    "#    \"f\": lambda xs: sphere( 0.5, xs),\n",
    "#    \"minimization\": True\n",
    "   # put other parameters in here.\n",
    "# }\n",
    "```\n",
    "\n",
    "and then have your code check for \"minimization\" and create an entry for \"fitness\" that is appropriate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def sphere(shift, xs):\n",
    "    return sum( [(x - shift)**2 for x in xs])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "113.42720000000001"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sphere( 0.5, [1.0, 2.0, -3.4, 5.0, -1.2, 3.23, 2.87, -4.23, 3.82, -4.61])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**convert_binary_individual_to_real**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def convert_binary_individual_to_real(individual: List[int]) -> float:\n",
    "    \"\"\" Returns the float value of a binary encoded individual (-5.11 to 5.11)\n",
    "\n",
    "    Args:\n",
    "        individual: Binary encoded individual\n",
    "\n",
    "    Returns:\n",
    "        Float value of the binary encoded individual\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    return normalize_binary_encoding(sum(individual[i] * 512 / 2 ** i for i in range(0, 10)))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**normalize_binary_encoding**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def normalize_binary_encoding(value: int) -> float:\n",
    "    \"\"\" Converts the binary value (0 to 1023) to a float value (-5.12 to 5.11)\n",
    "\n",
    "    Args:\n",
    "        value: An integer value from 0 to 1023\n",
    "\n",
    "    Returns:\n",
    "        Float value from -5.12 to 5.11\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    return (value - 512) / 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**generate_random_binary_individual**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def generate_random_binary_individual() -> List[int]:\n",
    "    \"\"\" Returns a list that contains 10 randomly initialized binary individuals in a single list.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    return [random.randint(0, 1) for i in range(100)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**convert_binary_genotype_to_phenotype**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def convert_binary_genotype_to_phenotype(genotype: List[int]) -> List[float]:\n",
    "    \"\"\" Converts a binary phenotype to a genotype.\n",
    "\n",
    "    Args:\n",
    "        genotype: Genotype, containing 1's and 0's\n",
    "\n",
    "    Returns:\n",
    "        List containing ten floats representing the phenotype.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    return [convert_binary_individual_to_real(genotype[i * 10:(i + 1) * 10]) for i in range(10)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**generate_random_real_individual**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def generate_random_real_individual() -> List[float]:\n",
    "    \"\"\" Generates a list that contains 10 randomly initialized real individuals in a single list.\n",
    "\n",
    "    Returns:\n",
    "        List containing 10 random floats between -5.12 and 5.12.\n",
    "\n",
    "    \"\"\"\n",
    "    return [random.uniform(-5.12, 5.12) for i in range(10)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**evaluate**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def evaluate(parameters, best_genotype: List[int], best_phenotype: List[float], best_fitness: float):\n",
    "    \"\"\" Evaluates the fitness of the given population. If it is better, the given population is set as the \"best\".\n",
    "\n",
    "    Args:\n",
    "        parameters: Dict of common parameters for the population, including genotype & phenotype\n",
    "        best_genotype: The best genotype observed thus far for the given population\n",
    "        best_phenotype: The best phenotype observed thus far for the given population\n",
    "        best_fitness: The best fitness observed thus far for the given population\n",
    "\n",
    "    Returns:\n",
    "        Tuple containing the best genotype, best phenotype, and best fitness value.\n",
    "\n",
    "    \"\"\"\n",
    "    f_value = parameters[\"f\"](parameters[\"phenotype\"])\n",
    "    if parameters[\"minimization\"]:\n",
    "        fitness = 1 / (1 + f_value)\n",
    "    else:\n",
    "        fitness = f_value\n",
    "\n",
    "    if fitness > best_fitness:\n",
    "        best_fitness = fitness\n",
    "        best_genotype = parameters[\"population\"]\n",
    "        best_phenotype = parameters[\"phenotype\"]\n",
    "\n",
    "    return best_genotype, best_phenotype, best_fitness"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**pick_parents**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def pick_parents(parameters):\n",
    "    \"\"\" Function to choose 5 sets of parents in a random fashion, but weighted according to each parents' fitness.\n",
    "\n",
    "    The fitness of each parent in the current generation is assessed by looking at each phenotype's proportion of\n",
    "    1 / (1 + f). Then, two parents are chosen at random, with replacement, according to the probabilities of their\n",
    "    weights of the total fitness.\n",
    "\n",
    "    Args:\n",
    "        parameters: Dict of common parameters for the population, including genotype & phenotype\n",
    "\n",
    "    Returns:\n",
    "        Tuple containing two parents genotypes.\n",
    "\n",
    "    \"\"\"\n",
    "    weighted_phenotypes = []\n",
    "    fitness_proportions = []\n",
    "    n = len(parameters[\"phenotype\"])\n",
    "    for i in range(n):\n",
    "        if parameters[\"minimization\"]:\n",
    "            weighted_phenotypes.append(1 / (1 + parameters[\"f\"]([(parameters[\"phenotype\"][i])])))\n",
    "        else:\n",
    "            weighted_phenotypes.append(parameters[\"f\"]([(parameters[\"phenotype\"][i])]))\n",
    "\n",
    "    sum_of_weighted_phenotypes = sum(weighted_phenotypes)\n",
    "\n",
    "    for i in range(n):\n",
    "        fitness_proportions.append(weighted_phenotypes[i] / sum_of_weighted_phenotypes)\n",
    "\n",
    "    # choose a parent randomly, but according to the parents' proportion of fitness\n",
    "    parent1_index = random.choices(range(n), fitness_proportions)[0]\n",
    "    parent2_index = random.choices(range(n), fitness_proportions)[0]\n",
    "\n",
    "    # assign parents\n",
    "    parent1 = parameters[\"get_individual_from_population\"](parent1_index, parameters[\"population\"])\n",
    "    parent2 = parameters[\"get_individual_from_population\"](parent2_index, parameters[\"population\"])\n",
    "    return parent1, parent2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**mutate_binary_child**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def mutate_binary_child(child: List[int]) -> List[int]:\n",
    "    \"\"\" Mutates a binary child by choosing an index at random and negating its value.\n",
    "\n",
    "    Args:\n",
    "        child: The child to mutate.\n",
    "\n",
    "    Returns:\n",
    "        The mutated binary child.\n",
    "\n",
    "    \"\"\"\n",
    "    random_index = random.randint(0, 9)\n",
    "    child[random_index] = int(not child[random_index])\n",
    "    return child\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**crossover_binary_children**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def crossover_binary_children(child1: List[int], child2: List[int]):\n",
    "    \"\"\" Crossover function for the binary population that swaps the bit from the same index for two children.\n",
    "\n",
    "    Args:\n",
    "        child1: a genetic algorithm child\n",
    "        child2: another genetic algorithm child\n",
    "\n",
    "    Returns:\n",
    "        A tuple containing both children with one bit from the same index swapped from each.\n",
    "\n",
    "    \"\"\"\n",
    "    random_index = random.randint(0, 9)\n",
    "    child1_bit = child1[random_index]\n",
    "    child2_bit = child2[random_index]\n",
    "    child1[random_index] = child2_bit\n",
    "    child2[random_index] = child1_bit\n",
    "    return child1, child2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**mutate_real_child**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def mutate_real_child(child: float):\n",
    "    \"\"\" Mutates a floating point child by generating Gaussian noise and applying it to the current child.\n",
    "\n",
    "    If the value that results happens to be outside of the bounds of -5.12 <= x <= 5.12, then keep generating\n",
    "    possibilities until we find out that is in bounds.\n",
    "\n",
    "    Args:\n",
    "        child: A real child to whom Gaussian noise will be applied.\n",
    "\n",
    "    Returns:\n",
    "        A new child that has had Gaussian noise applied to it.\n",
    "    \"\"\"\n",
    "\n",
    "    # apply \"Gaussian noise\"\n",
    "    new_child = child + random.gauss(0, 1)\n",
    "\n",
    "    # make sure that the value that we generated is within the confines of our problem space\n",
    "    while not (-5.12 <= new_child <= 5.12):\n",
    "        new_child = child + random.gauss(0, 1)\n",
    "    return new_child"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**reproduce**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "pycharm": {
     "is_executing": false,
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "def reproduce(parameters, parents: Tuple[float, float]) -> Tuple[float, float]:\n",
    "    \"\"\" Function that takes two parents, applies crossover and mutation and returns two children.\n",
    "\n",
    "    The probability of crossover is 95%.\n",
    "    The probability of mutation is 5% for each child.\n",
    "\n",
    "    Args:\n",
    "        parameters,\n",
    "        parent1: real parent #1\n",
    "        parent2: real parent #2\n",
    "\n",
    "    Returns:\n",
    "        A tuple containing the two children.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    child1: float = deepcopy(parents[0])\n",
    "    child2: float = deepcopy(parents[1])\n",
    "\n",
    "    if random.random() < parameters[\"prob_of_crossover\"]:\n",
    "        # crossover the two children\n",
    "        child1, child2 = parameters[\"crossover_function\"]((child2, child1))\n",
    "    if random.random() < parameters[\"prob_of_mutation\"]:\n",
    "        # mutate child1\n",
    "        child1 = parameters[\"mutation_function\"](child1)\n",
    "    if random.random() < parameters[\"prob_of_mutation\"]:\n",
    "        # mutate child2\n",
    "        child2 = parameters[\"mutation_function\"](child2)\n",
    "    return child1, child2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**execute_ga**\n",
    "\n",
    "Function to perform genetic algorithm for either a binary or real number based population."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "## Traditional GA\n",
    "## binary_ga( parameters)\n",
    "def execute_ga(parameters) -> Tuple[List[float], List[float], float]:\n",
    "    \"\"\" Function to run the shifting spheres genetic algorithm on a number population.\n",
    "\n",
    "    The function runs 200,000 generations on a real valued population where the genotype and phenotype are identical.\n",
    "\n",
    "    The evaluation function determines the fitness of the current generation and sets it to the best if it is greater\n",
    "    than the previous best value.\n",
    "\n",
    "    Then, ten new individuals are chosen by randomly reproducing the current individuals according to their proportion\n",
    "    of the total fitness thus far.\n",
    "\n",
    "    When the randomly chosen individuals reproduce, they crossover and mutate according to their defined probabilities.\n",
    "\n",
    "    At the end of each generation, the new generation is set and the process continues until all the generations\n",
    "    execute.\n",
    "\n",
    "    Finally, the best individual is returned.\n",
    "\n",
    "\n",
    "    Args:\n",
    "        parameters: Dict of common parameters for the population, including genotype & phenotype\n",
    "\n",
    "    Returns:\n",
    "        Tuple containing best individual's genotype, phenotype, and fitness.\n",
    "    \"\"\"\n",
    "    generations: int = 0\n",
    "    limit: int = 200_000\n",
    "    best_genotype = []\n",
    "    best_phenotype = []\n",
    "    best_fitness = -sys.maxsize\n",
    "    while generations < limit:\n",
    "\n",
    "        best_genotype, best_phenotype, best_fitness = evaluate(parameters, best_genotype, best_phenotype,\n",
    "                                                               best_fitness)\n",
    "        next_population = []\n",
    "        for n in range(0, int(len(parameters[\"phenotype\"]) / 2)):\n",
    "            parent1, parent2 = pick_parents(parameters)\n",
    "            child1, child2 = reproduce(parameters, (parent1, parent2))\n",
    "            parameters[\"add_to_new_population\"](next_population, child1)\n",
    "            parameters[\"add_to_new_population\"](next_population, child2)\n",
    "        parameters[\"population\"] = next_population\n",
    "        parameters[\"phenotype\"] = parameters[\"convert_geno_to_pheno\"](parameters[\"population\"])\n",
    "        generations += 1\n",
    "\n",
    "        if debug and generations % (limit / 20) == 0:\n",
    "            print(f'Generation #{generations}')\n",
    "            print('Best Individual:')\n",
    "            print(f'Genotype:\\t{best_genotype}')\n",
    "            print(f'Phenotype:\\t{best_phenotype}')\n",
    "            print(f'Fitness:\\t{best_fitness}')\n",
    "            print(f'Function value:\\t{parameters[\"f\"](best_genotype)}')\n",
    "\n",
    "    return best_genotype, best_phenotype, best_fitness"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Traditional (Binary) GA\n",
    "\n",
    "## Outputs the best individual from 200,000 generations of the genetic algorithm.\n",
    "\n",
    "### If debug flag is set, 20 debug messages will be printed throughout the execution of the algorithm stating the best individual so far.\n",
    "\n",
    "### The probability of crossover is 95%.\n",
    "\n",
    "### The probability of mutation is 24%.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "pycharm": {
     "is_executing": true,
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Executing Binary GA\n",
      "Final Result\n",
      "Best Individual:\n",
      "Genotype:\t[1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0]\n",
      "Phenotype:\t[0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]\n",
      "Fitness:\t1.0\n",
      "Function value:\t0.0\n",
      " \n"
     ]
    }
   ],
   "source": [
    "# Set debug flag here\n",
    "debug = False\n",
    "binary_population = generate_random_binary_individual()\n",
    "parameters = {  # should include \"at least\" genome (float value) and fitness score\n",
    "        \"f\": lambda xs: sphere(0.5, xs),  # lambda is expecting phenotype (list of floats)\n",
    "        \"minimization\": True,\n",
    "        \"population\": binary_population,\n",
    "        \"phenotype\": convert_binary_genotype_to_phenotype(binary_population),\n",
    "        \"prob_of_crossover\": 0.95,\n",
    "        \"prob_of_mutation\": 0.24,\n",
    "        \"crossover_function\": lambda children: crossover_binary_children(children[1], children[0]),\n",
    "        \"mutation_function\": lambda child: mutate_binary_child(child),\n",
    "        \"add_to_new_population\": lambda population, child: population.extend(child),\n",
    "        \"convert_geno_to_pheno\": lambda genotype: convert_binary_genotype_to_phenotype(genotype),\n",
    "        \"get_individual_from_population\": lambda index, population: population[index * 10:(index + 1) * 10]\n",
    "}\n",
    "\n",
    "print('Executing Binary GA')\n",
    "best_binary_genotype, best_binary_phenotype, best_real_fitness = execute_ga(parameters)\n",
    "print('Final Result')\n",
    "print('Best Individual:')\n",
    "print(f'Genotype:\\t{best_binary_genotype}')\n",
    "print(f'Phenotype:\\t{best_binary_phenotype}')\n",
    "print(f'Fitness:\\t{best_real_fitness}')\n",
    "print(f'Function value:\\t{parameters[\"f\"](best_binary_phenotype)}')\n",
    "print(' ')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Real GA\n",
    "\n",
    "## Outputs the best individual from 200,000 generations of the genetic algorithm.\n",
    "\n",
    "### If debug flag is set, 20 debug messages will be printed throughout the execution of the algorithm stating the best individual so far.\n",
    "\n",
    "### The probability of crossover is 90%.\n",
    "\n",
    "### The probability of mutation is 5%."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true,
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Executing Real GA\n"
     ]
    }
   ],
   "source": [
    "# Set debug flag here\n",
    "debug = False\n",
    "real_population = generate_random_real_individual()\n",
    "parameters = {\n",
    "        \"f\": lambda xs: sphere(0.5, xs),\n",
    "        \"minimization\": True,\n",
    "        \"population\": real_population,\n",
    "        \"phenotype\": real_population,\n",
    "        \"prob_of_crossover\": 0.90,\n",
    "        \"prob_of_mutation\": 0.05,\n",
    "        \"crossover_function\": lambda children: (children[1], children[0]),\n",
    "        \"mutation_function\": lambda child: mutate_real_child(child),\n",
    "        \"add_to_new_population\": lambda population, child: population.append(child),\n",
    "        \"convert_geno_to_pheno\": lambda genotype: genotype,\n",
    "        \"get_individual_from_population\": lambda index, population: population[index]\n",
    "}\n",
    "print('Executing Real GA')\n",
    "best_real_genotype, best_real_phenotype, best_real_fitness = execute_ga(parameters)\n",
    "print('Final Result')\n",
    "print('Best Individual:')\n",
    "print(f'Genotype:\\t{best_real_genotype}')\n",
    "print(f'Phenotype:\\t{best_real_phenotype}')\n",
    "print(f'Fitness:\\t{best_real_fitness}')\n",
    "print(f'Function value:\\t{parameters[\"f\"](best_real_phenotype)}')\n",
    "print('')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "metadata": {
     "collapsed": false
    },
    "source": []
   }
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "120px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
