• The initial state of the search is the initial state from the planning problem. In general,
each state will be a set of positive ground literals; literals not appearing are false.
384 Chapter 11. Planning
• The actions that are applicable to a state are all those whose preconditions are satisfied.
The successor state resulting from an action is generated by adding the positive effect
literals and deleting the negative effect literals. (In the first-order case, we must apply
the unifier from the preconditions to the effect literals.) Note that a single successor
function works for all planning problems—a consequence of using an explicit action
representation.
• The goal test checks whether the state satisfies the goal of the planning problem.
• The step cost of each action is typically 1. Although it would be easy to allow different
costs for different actions, this is seldom done by STRIPS planners