import sys
from copy import deepcopy
from typing import List, Dict

from unification import unify, parse


def add_to_current_state(add_list: List[str], matches: Dict[str, str], current_state: List[str]):
    """ Helper function to inject the add list into the provided state.

    Args:
        add_list: add list from the action
        matches: matches found through unification
        current_state: the current state
    """
    things_to_add = []
    things_to_add = inject_matches(add_list, matches)
    for thing_to_add in things_to_add:
        current_state.append(thing_to_add)


def remove_from_current_state(delete_list: List[str], matches: Dict[str, str], current_state: List[str]):
    """ Helper function to inject the delete list into the provided state.

    Args:
        delete_list: delete list from the action
        matches: matches found through unification
        current_state: the current state
    """
    things_to_delete = []
    things_to_delete = inject_matches(delete_list, matches)
    for thing_to_delete in things_to_delete:
        for state in current_state:
            if state == thing_to_delete:
                current_state.remove(state)


def inject_matches(item_list: List[str], matches: Dict[str, str]):
    """ Helper function to inject the matches found through unification into the provided list.

    Args:
        item_list: A list of prediates and variables.
        matches: A dict of matches found through unification

    Returns:
        The input list with its variables substituted using the matches dict.
    """
    injected_list = []
    for item in item_list:
        match_item = '('
        for token in parse(item):
            if token[0] == '?' and matches.get(token):
                match_item += (matches[token]) + ' '
            else:
                match_item += (token) + ' '
        match_item = match_item.strip() + ')'
        injected_list.append(match_item)
    return injected_list


def have_not_reached_goal(current_state: List[str], goal: List[str]):
    """ Helper function to return whether or not the goal state has been achieved.

    Args:
        current_state: The current state.
        goal: The goal state.

    Returns:
        True if the goal is not the current state, False otherwise.
    """
    for state in goal:
        if state not in current_state:
            return True
    return False


def forward_planner(start_state: List[str], goal: List[str], actions, debug=False):
    """ Helper function to set up variables and call our state space search function.

    Args:
        start_state: the starting state
        goal: the goal that we are searching for
        actions: a dict containing the provided actions
        debug: flag to enable debug info

    Returns:
        The plan to reach the goal from our starting state.
    """
    current_state = start_state
    plan = []
    explored_states = []
    # loop until we reach our goal
    while have_not_reached_goal(current_state, goal):
        plan = search_for_action(actions, current_state, explored_states, plan)
    return plan


def search_for_action(actions, current_state: List[str], explored_states: List[List[str]], plan: List[str]) -> List[
    str]:
    """ Recursive function that searches through the provided actions' predicates while
    unifying them with the current state. The function is modeled after the state space
    search pseudo-code for graph-search provided in the text on page 77.

    Args:
        actions: the action dict provided
        current_state: the current state
        explored_states: a list that contains all of the explored states
        plan: the plan of actions to take

    Returns:
        A list containing the plan to reach from the starting state to the goal.
        If debug is True, it will return the state after each action.
    """
    # loop through all of the potential actions
    for name, action in actions.items():
        volatile_current_state = deepcopy(current_state)
        action = deepcopy(actions[name])
        matches = {}
        # loop through each pre-condition of the action to make sure it matches our current state
        for test_precondition in action['conditions']:
            does_match = False
            test_uni = None
            # loop through each predicate in the current state
            for current_state_item in volatile_current_state:
                # test to see if precondition predicate unifies with current state predicate
                test_uni = unify(test_precondition, current_state_item)
                if test_uni:
                    does_match = True

                    # before adding the match, we'll copy the current state, remove that state and
                    # recurse, so we can find the rest of the matching states
                    recurs_current_state = deepcopy(current_state)
                    recurs_current_state.remove(current_state_item)
                    search_for_action(actions, recurs_current_state, explored_states,
                                      plan)
                    matches.update(test_uni)
                    action['conditions'] = inject_matches(action['conditions'], matches)

                    volatile_current_state.remove(current_state_item)

                    break
            if not test_uni:
                break
        if does_match:
            # store current state to explored list
            sorted_state = deepcopy(current_state)
            sorted_state.sort()
            explored_states.append(sorted_state)

            potential_plan = deepcopy(plan)
            # append action to the potential plan
            potential_plan.append(inject_matches([action["action"]], matches)[0])

            # modify the current state using the 'delete' list
            remove_from_current_state(action["delete"], matches, current_state)

            # modify the current state using the 'add' list
            add_to_current_state(action["add"], matches, current_state)

            # only update the plan if the resultant state has not already been explored
            sorted_state = deepcopy(current_state)
            sorted_state.sort()
            if (sorted_state not in explored_states):
                plan = potential_plan
                if debug:
                    plan.append(current_state)
    return plan


if __name__ == "__main__":
    # use this debug flag if necessary; otherwise, the program should print out only
    # what is requested.
    # global unifications
    # unifications = 0
    debug = len(sys.argv) > 1 and sys.argv[1].lower() == 'debug'
    start_state = [
            "(item Drill)",
            "(place Home)",
            "(place Store)",
            "(agent Me)",
            "(at Me Home)",
            "(at Drill Store)"
    ]
    goal = [
            "(item Drill)",
            "(place Home)",
            "(place Store)",
            "(agent Me)",
            "(at Me Home)",
            "(at Drill Me)"
    ]
    actions = {
            "drive": {
                    "action": "(drive ?agent ?from ?to)",
                    "conditions": [
                            "(agent ?agent)",
                            "(place ?from)",
                            "(place ?to)",
                            "(at ?agent ?from)"
                    ],
                    "add": [
                            "(at ?agent ?to)"
                    ],
                    "delete": [
                            "(at ?agent ?from)"
                    ]
            },
            "buy": {
                    "action": "(buy ?purchaser ?seller ?item)",
                    "conditions": [
                            "(item ?item)",
                            "(place ?seller)",
                            "(agent ?purchaser)",
                            "(at ?item ?seller)",
                            "(at ?purchaser ?seller)"
                    ],
                    "add": [
                            "(at ?item ?purchaser)"
                    ],
                    "delete": [
                            "(at ?item ?seller)"
                    ]
            }
    }

    plan = forward_planner(start_state, goal, actions)
    print(plan)
