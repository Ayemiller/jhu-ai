{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Module 7 - Programming Assignment\n",
    "\n",
    "## Directions\n",
    "\n",
    "There are general instructions on Blackboard and in the Syllabus for Programming Assignments. This Notebook also has instructions specific to this assignment. Read all the instructions carefully and make sure you understand them. Please ask questions on the discussion boards or email me at `EN605.445@gmail.com` if you do not understand something.\n",
    "\n",
    "<div style=\"background: mistyrose; color: firebrick; border: 2px solid darkred; padding: 5px; margin: 10px;\">\n",
    "Please follow the directions and make sure you provide the requested output. Failure to do so may result in a lower grade even if the code is correct or even 0 points.\n",
    "</div>\n",
    "\n",
    "You must submit your assignment as `<jhed_id>.ipynb`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Forward Planner\n",
    "\n",
    "## Unify\n",
    "\n",
    "Use the accompanying `unification.py` file for unification. For this assignment, you're almost certainly going to want to be able to:\n",
    "\n",
    "1. specify the problem in terms of S-expressions.\n",
    "2. parse them.\n",
    "3. work with the parsed versions.\n",
    "\n",
    "`parse` and `unification` work exactly like the programming assignment for last time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "from copy import deepcopy\n",
    "from typing import List, Dict\n",
    "from unification import parse, unification, unify\n",
    "import sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Forward Planner\n",
    "\n",
    "In this assigment, you're going to implement a Forward Planner. What does that mean? If you look in your book, you will not find pseudocode for a forward planner. It just says \"use state space search\" but this is less than helpful and it's a bit more complicated than that. **(but please please do not try to implement STRIPS or GraphPlan...that is wrong).**\n",
    "\n",
    "At a high level, a forward planner takes the current state of the world $S_0$ and attempts to derive a plan, basically by Depth First Search. We have all the ingredients we said we would need in Module 1: states, actions, a transition function and a goal test. We have a set of predicates that describe a state (and therefore all possible states), we have actions and we have, at least, an implicit transition function: applying an action in a state causes the state to change as described by the add and delete lists.\n",
    "\n",
    "Let's say we have a drill that's an item, two places such as home and store, and we know that I'm at home and the drill is at the store and I want to go buy a drill (have it be at home). We might represent that as:\n",
    "\n",
    "<code>\n",
    "start_state = [\n",
    "    \"(item Drill)\",\n",
    "    \"(place Home)\",\n",
    "    \"(place Store)\",\n",
    "    \"(agent Me)\",\n",
    "    \"(at Me Home)\",\n",
    "    \"(at Drill Store)\"\n",
    "]\n",
    "</code>\n",
    "\n",
    "And we have a goal state:\n",
    "\n",
    "<code>\n",
    "goal = [\n",
    "    \"(item Drill)\",\n",
    "    \"(place Home)\",\n",
    "    \"(place Store)\",\n",
    "    \"(agent Me)\",\n",
    "    \"(at Me Home)\",\n",
    "    \"(at Drill Me)\"\n",
    "]\n",
    "</code>\n",
    "\n",
    "The actions/operators are:\n",
    "\n",
    "<code>\n",
    "actions = {\n",
    "    \"drive\": {\n",
    "        \"action\": \"(drive ?agent ?from ?to)\",\n",
    "        \"conditions\": [\n",
    "            \"(agent ?agent)\",\n",
    "            \"(place ?from)\",\n",
    "            \"(place ?to)\",\n",
    "            \"(at ?agent ?from)\"\n",
    "        ],\n",
    "        \"add\": [\n",
    "            \"(at ?agent ?to)\"\n",
    "        ],\n",
    "        \"delete\": [\n",
    "            \"(at ?agent ?from)\"\n",
    "        ]\n",
    "    },\n",
    "    \"buy\": {\n",
    "        \"action\": \"(buy ?purchaser ?seller ?item)\",\n",
    "        \"conditions\": [\n",
    "            \"(item ?item)\",\n",
    "            \"(place ?seller)\",\n",
    "            \"(agent ?purchaser)\",\n",
    "            \"(at ?item ?seller)\",\n",
    "            \"(at ?purchaser ?seller)\"\n",
    "        ],\n",
    "        \"add\": [\n",
    "            \"(at ?item ?purchaser)\"\n",
    "        ],\n",
    "        \"delete\": [\n",
    "            \"(at ?item ?seller)\"\n",
    "        ]\n",
    "    }\n",
    "}\n",
    "</code>\n",
    "\n",
    "These will all need to be parsed from s-expressions to the underlying Python representation before you can use them. You might as well do it at the start of your algorithm, once. The order of the conditions is *not* arbitrary. It is much, much better for the unification and backtracking if you have the \"type\" predicates (item, place, agent) before the more complex ones. Trust me on this.\n",
    "\n",
    "As for the algorithm itself, there is going to be an *outer* level of search and an *inner* level of search.\n",
    "\n",
    "The *outer* level of search that is exactly what I describe here: you have a state, you generate successor states by applying actions to the current state, you examine those successor states as we did at the first week of the semester and if one is the goal you stop, if you see a repeat state, you put it on the explored list (you should implement graph search not tree search). What could be simpler?\n",
    "\n",
    "It turns out the Devil is in the details. There is an *inner* level of search hidden in \"you generate successor states by applying actions to the current state\". Where?\n",
    "\n",
    "How do you know if an action applies in a state? Only if the preconditions successfully unify with the current state. That seems easy enough...you check each predicate in the conditions to see if it unifies with the current state and if it does, you use the substitution list on the action, the add and delete lists and create the successor state based on them.\n",
    "\n",
    "Except for one small problem...there may be more than one way to unify an action with the current state. You must essentially search for all successful unifications of the candidate action and the current state. This is where my question through the semester applies, \"how would you modify state space search to return all the paths to the goal?\"\n",
    "\n",
    "Unification can be seen as state space search by trying to unify the first precondition with the current state, progressively working your way through the precondition list. If you fail at any point, you may need to backtrack because there might have been another unification of that predicate that would succeed. Similarly, as already mentioned, there may be more than one.\n",
    "\n",
    "So...by using unification and a properly defined <code>successors</code> function, you should be able to apply graph based search to the problem and return a \"path\" through the states from the initial state to the goal. You'll definitely want to use graph-based search since <code>( drive Me Store), (drive Me Home), (drive Me Store), (drive Me Home), (drive Me Store), (buy Me Store Drill), (drive Me Home)</code> is a valid plan.\n",
    "\n",
    "Your function should return the plan...but if you pass an extra debug=True parameter, it should also return the intermediate *states* as well as the actions.\n",
    "\n",
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "**inject_matches**"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "outputs": [],
   "source": [
    "def inject_matches(item_list: List[str], matches: Dict[str, str]):\n",
    "    \"\"\" Helper function to inject the matches found through unification into the provided list.\n",
    "\n",
    "    Args:\n",
    "        item_list: A list of prediates and variables.\n",
    "        matches: A dict of matches found through unification\n",
    "\n",
    "    Returns:\n",
    "        The input list with its variables substituted using the matches dict.\n",
    "    \"\"\"\n",
    "    injected_list = []\n",
    "    for item in item_list:\n",
    "        match_item = '('\n",
    "        for token in parse(item):\n",
    "            if token[0] == '?' and matches.get(token):\n",
    "                match_item += (matches[token]) + ' '\n",
    "            else:\n",
    "                match_item += (token) + ' '\n",
    "        match_item = match_item.strip() + ')'\n",
    "        injected_list.append(match_item)\n",
    "    return injected_list"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**add_to_current_state**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def add_to_current_state(add_list: List[str], matches: Dict[str, str], current_state: List[str]):\n",
    "    \"\"\" Helper function to inject the add list into the provided state.\n",
    "\n",
    "    Args:\n",
    "        add_list: add list from the action\n",
    "        matches: matches found through unification\n",
    "        current_state: the current state\n",
    "    \"\"\"\n",
    "    things_to_add = []\n",
    "    things_to_add = inject_matches(add_list, matches)\n",
    "    for thing_to_add in things_to_add:\n",
    "        current_state.append(thing_to_add)"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "**remove_from_current_state**"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "outputs": [],
   "source": [
    "def remove_from_current_state(delete_list: List[str], matches: Dict[str, str], current_state: List[str]):\n",
    "    \"\"\" Helper function to inject the delete list into the provided state.\n",
    "\n",
    "    Args:\n",
    "        delete_list: delete list from the action\n",
    "        matches: matches found through unification\n",
    "        current_state: the current state\n",
    "    \"\"\"\n",
    "    things_to_delete = []\n",
    "    things_to_delete = inject_matches(delete_list, matches)\n",
    "    for thing_to_delete in things_to_delete:\n",
    "        for state in current_state:\n",
    "            if state == thing_to_delete:\n",
    "                current_state.remove(state)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**have_not_reached_goal**"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "outputs": [],
   "source": [
    "def have_not_reached_goal(current_state: List[str], goal: List[str]):\n",
    "    \"\"\" Helper function to return whether or not the goal state has been achieved.\n",
    "\n",
    "    Args:\n",
    "        current_state: The current state.\n",
    "        goal: The goal state.\n",
    "\n",
    "    Returns:\n",
    "        True if the goal is not the current state, False otherwise.\n",
    "    \"\"\"\n",
    "    for state in goal:\n",
    "        if state not in current_state:\n",
    "            return True\n",
    "    return False"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**search_for_action**"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "outputs": [],
   "source": [
    "def search_for_action(actions, current_state: List[str], explored_states: List[List[str]], plan: List[str], debug: bool) -> List[\n",
    "    str]:\n",
    "    \"\"\" Recursive function that searches through the provided actions' predicates while\n",
    "    unifying them with the current state. The function is modeled after the state space\n",
    "    search pseudo-code for graph-search provided in the text on page 77.\n",
    "\n",
    "    Args:\n",
    "        actions: the action dict provided\n",
    "        current_state: the current state\n",
    "        explored_states: a list that contains all of the explored states\n",
    "        plan: the plan of actions to take\n",
    "        debug: flag to include states with plan \n",
    "    Returns:\n",
    "        A list containing the plan to reach from the starting state to the goal.\n",
    "        If debug is True, it will return the state after each action.\n",
    "    \"\"\"\n",
    "    # loop through all of the potential actions\n",
    "    for name, action in actions.items():\n",
    "        volatile_current_state = deepcopy(current_state)\n",
    "        action = deepcopy(actions[name])\n",
    "        matches = {}\n",
    "        # loop through each pre-condition of the action to make sure it matches our current state\n",
    "        for test_precondition in action['conditions']:\n",
    "            does_match = False\n",
    "            test_uni = None\n",
    "            # loop through each predicate in the current state\n",
    "            for current_state_item in volatile_current_state:\n",
    "                # test to see if precondition predicate unifies with current state predicate\n",
    "                test_uni = unify(test_precondition, current_state_item)\n",
    "                if test_uni:\n",
    "                    does_match = True\n",
    "\n",
    "                    # before adding the match, we'll copy the current state, remove that state and\n",
    "                    # recurse, so we can find the rest of the matching states\n",
    "                    recurs_current_state = deepcopy(current_state)\n",
    "                    recurs_current_state.remove(current_state_item)\n",
    "                    search_for_action(actions, recurs_current_state, explored_states,\n",
    "                                      plan, debug)\n",
    "                    matches.update(test_uni)\n",
    "                    action['conditions'] = inject_matches(action['conditions'], matches)\n",
    "\n",
    "                    volatile_current_state.remove(current_state_item)\n",
    "\n",
    "                    break\n",
    "            if not test_uni:\n",
    "                break\n",
    "        if does_match:\n",
    "            # store current state to explored list\n",
    "            sorted_state = deepcopy(current_state)\n",
    "            sorted_state.sort()\n",
    "            explored_states.append(sorted_state)\n",
    "\n",
    "            potential_plan = deepcopy(plan)\n",
    "            # append action to the potential plan\n",
    "            potential_plan.append(inject_matches([action[\"action\"]], matches)[0])\n",
    "\n",
    "            # modify the current state using the 'delete' list\n",
    "            remove_from_current_state(action[\"delete\"], matches, current_state)\n",
    "\n",
    "            # modify the current state using the 'add' list\n",
    "            add_to_current_state(action[\"add\"], matches, current_state)\n",
    "\n",
    "            # only update the plan if the resultant state has not already been explored\n",
    "            sorted_state = deepcopy(current_state)\n",
    "            sorted_state.sort()\n",
    "            if (sorted_state not in explored_states):\n",
    "                plan = potential_plan\n",
    "                if debug:\n",
    "                    plan.append(current_state)\n",
    "    return plan\n",
    "\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(you can just overwrite that one and add as many others as you need). Remember to follow the **Guidelines**.\n",
    "\n",
    "\n",
    "-----\n",
    "\n",
    "So you need to implement `forward_planner` as described above. `start_state`, `goal` and `actions` should all have the layout above and be s-expressions.\n",
    "\n",
    "Your implementation should return the plan as a **List of instantiated actions**. If `debug=True`, you should print out the intermediate states of the plan as well."
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "**forward_planner**\n",
    "\n",
    "This forward planner uses a recursive function (search_for_action, above) that searches through the provided actions' predicates while unifying them with the current state. The function is modeled after the state space search pseudo-code for graph-search provided in the text on page 77.\n",
    "\n",
    "**I was unable to create the fully functioning state-space search algorithm.** I believe that I am missing something minor in the way that I am identifying already explored states. The algorithm runs forever currently, because it will not find a valid plan, however all of the helper functions (s-expressions, parsing, unification, and variable substitution) work properly.\n",
    " "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def forward_planner(start_state: List[str], goal: List[str], actions, debug=False):\n",
    "    \"\"\" Helper function to set up variables and call our state space search function.\n",
    "\n",
    "    Args:\n",
    "        start_state: the starting state\n",
    "        goal: the goal that we are searching for\n",
    "        actions: a dict containing the provided actions\n",
    "        debug: flag to enable debug info\n",
    "\n",
    "    Returns:\n",
    "        The plan to reach the goal from our starting state.\n",
    "    \"\"\"\n",
    "    current_state = start_state\n",
    "    plan = []\n",
    "    explored_states = []\n",
    "    # loop until we reach our goal\n",
    "    while have_not_reached_goal(current_state, goal):\n",
    "        plan = search_for_action(actions, current_state, explored_states, plan, debug)\n",
    "    return plan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will be solving the problem from above. Here is the start state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "start_state = [\n",
    "    \"(item Drill)\",\n",
    "    \"(place Home)\",\n",
    "    \"(place Store)\",\n",
    "    \"(agent Me)\",\n",
    "    \"(at Me Home)\",\n",
    "    \"(at Drill Store)\"\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The goal state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "goal = [\n",
    "    \"(item Drill)\",\n",
    "    \"(place Home)\",\n",
    "    \"(place Store)\",\n",
    "    \"(agent Me)\",\n",
    "    \"(at Me Home)\",\n",
    "    \"(at Drill Me)\"\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and the actions/operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "actions = {\n",
    "    \"drive\": {\n",
    "        \"action\": \"(drive ?agent ?from ?to)\",\n",
    "        \"conditions\": [\n",
    "            \"(agent ?agent)\",\n",
    "            \"(place ?from)\",\n",
    "            \"(place ?to)\",\n",
    "            \"(at ?agent ?from)\"\n",
    "        ],\n",
    "        \"add\": [\n",
    "            \"(at ?agent ?to)\"\n",
    "        ],\n",
    "        \"delete\": [\n",
    "            \"(at ?agent ?from)\"\n",
    "        ]\n",
    "    },\n",
    "    \"buy\": {\n",
    "        \"action\": \"(buy ?purchaser ?seller ?item)\",\n",
    "        \"conditions\": [\n",
    "            \"(item ?item)\",\n",
    "            \"(place ?seller)\",\n",
    "            \"(agent ?purchaser)\",\n",
    "            \"(at ?item ?seller)\",\n",
    "            \"(at ?purchaser ?seller)\"\n",
    "        ],\n",
    "        \"add\": [\n",
    "            \"(at ?item ?purchaser)\"\n",
    "        ],\n",
    "        \"delete\": [\n",
    "            \"(at ?item ?seller)\"\n",
    "        ]\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [
    {
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mKeyboardInterrupt\u001b[0m                         Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-11-286dc538a399>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mplan\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mforward_planner\u001b[0m\u001b[1;33m(\u001b[0m \u001b[0mstart_state\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mgoal\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mactions\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m      2\u001b[0m \u001b[0mprint\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mplan\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m      3\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m<ipython-input-7-c5bd17d72c1f>\u001b[0m in \u001b[0;36mforward_planner\u001b[1;34m(start_state, goal, actions, debug)\u001b[0m\n\u001b[0;32m     16\u001b[0m     \u001b[1;31m# loop until we reach our goal\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     17\u001b[0m     \u001b[1;32mwhile\u001b[0m \u001b[0mhave_not_reached_goal\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mgoal\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 18\u001b[1;33m         \u001b[0mplan\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0msearch_for_action\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mactions\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mcurrent_state\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mexplored_states\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mplan\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mdebug\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m     19\u001b[0m     \u001b[1;32mreturn\u001b[0m \u001b[0mplan\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     20\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m<ipython-input-6-d15eb49cdf2a>\u001b[0m in \u001b[0;36msearch_for_action\u001b[1;34m(actions, current_state, explored_states, plan, debug)\u001b[0m\n\u001b[0;32m     35\u001b[0m                     \u001b[0mrecurs_current_state\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mdeepcopy\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     36\u001b[0m                     \u001b[0mrecurs_current_state\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mremove\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state_item\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 37\u001b[1;33m                     search_for_action(actions, recurs_current_state, explored_states,\n\u001b[0m\u001b[0;32m     38\u001b[0m                                       plan, debug)\n\u001b[0;32m     39\u001b[0m                     \u001b[0mmatches\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mupdate\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtest_uni\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m<ipython-input-6-d15eb49cdf2a>\u001b[0m in \u001b[0;36msearch_for_action\u001b[1;34m(actions, current_state, explored_states, plan, debug)\u001b[0m\n\u001b[0;32m     35\u001b[0m                     \u001b[0mrecurs_current_state\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mdeepcopy\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     36\u001b[0m                     \u001b[0mrecurs_current_state\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mremove\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state_item\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 37\u001b[1;33m                     search_for_action(actions, recurs_current_state, explored_states,\n\u001b[0m\u001b[0;32m     38\u001b[0m                                       plan, debug)\n\u001b[0;32m     39\u001b[0m                     \u001b[0mmatches\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mupdate\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtest_uni\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m<ipython-input-6-d15eb49cdf2a>\u001b[0m in \u001b[0;36msearch_for_action\u001b[1;34m(actions, current_state, explored_states, plan, debug)\u001b[0m\n\u001b[0;32m     35\u001b[0m                     \u001b[0mrecurs_current_state\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mdeepcopy\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     36\u001b[0m                     \u001b[0mrecurs_current_state\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mremove\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state_item\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 37\u001b[1;33m                     search_for_action(actions, recurs_current_state, explored_states,\n\u001b[0m\u001b[0;32m     38\u001b[0m                                       plan, debug)\n\u001b[0;32m     39\u001b[0m                     \u001b[0mmatches\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mupdate\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtest_uni\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m<ipython-input-6-d15eb49cdf2a>\u001b[0m in \u001b[0;36msearch_for_action\u001b[1;34m(actions, current_state, explored_states, plan, debug)\u001b[0m\n\u001b[0;32m     38\u001b[0m                                       plan, debug)\n\u001b[0;32m     39\u001b[0m                     \u001b[0mmatches\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mupdate\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtest_uni\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 40\u001b[1;33m                     \u001b[0maction\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;34m'conditions'\u001b[0m\u001b[1;33m]\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0minject_matches\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0maction\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;34m'conditions'\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mmatches\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m     41\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     42\u001b[0m                     \u001b[0mvolatile_current_state\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mremove\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mcurrent_state_item\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m<ipython-input-2-b85f7b1f30b5>\u001b[0m in \u001b[0;36minject_matches\u001b[1;34m(item_list, matches)\u001b[0m\n\u001b[0;32m     12\u001b[0m     \u001b[1;32mfor\u001b[0m \u001b[0mitem\u001b[0m \u001b[1;32min\u001b[0m \u001b[0mitem_list\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     13\u001b[0m         \u001b[0mmatch_item\u001b[0m \u001b[1;33m=\u001b[0m \u001b[1;34m'('\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 14\u001b[1;33m         \u001b[1;32mfor\u001b[0m \u001b[0mtoken\u001b[0m \u001b[1;32min\u001b[0m \u001b[0mparse\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mitem\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m     15\u001b[0m             \u001b[1;32mif\u001b[0m \u001b[0mtoken\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;36m0\u001b[0m\u001b[1;33m]\u001b[0m \u001b[1;33m==\u001b[0m \u001b[1;34m'?'\u001b[0m \u001b[1;32mand\u001b[0m \u001b[0mmatches\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mget\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtoken\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     16\u001b[0m                 \u001b[0mmatch_item\u001b[0m \u001b[1;33m+=\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0mmatches\u001b[0m\u001b[1;33m[\u001b[0m\u001b[0mtoken\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m)\u001b[0m \u001b[1;33m+\u001b[0m \u001b[1;34m' '\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m~\\Documents\\jhu\\ai\\module7\\unification.py\u001b[0m in \u001b[0;36mparse\u001b[1;34m(exp)\u001b[0m\n\u001b[0;32m     95\u001b[0m     \u001b[0msrc\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mStringIO\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mexp\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mreadline\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     96\u001b[0m     \u001b[0mtokens\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mtokenize\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mgenerate_tokens\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0msrc\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 97\u001b[1;33m     \u001b[1;32mreturn\u001b[0m \u001b[0matom\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtokens\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__next__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mtokens\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__next__\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m     98\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     99\u001b[0m \u001b[1;32mdef\u001b[0m \u001b[0munify\u001b[0m\u001b[1;33m(\u001b[0m \u001b[0mexp1\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mexp2\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32mC:\\Program Files\\Python38\\lib\\tokenize.py\u001b[0m in \u001b[0;36m_tokenize\u001b[1;34m(readline, encoding)\u001b[0m\n\u001b[0;32m    525\u001b[0m             \u001b[0mpseudomatch\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0m_compile\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mPseudoToken\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mmatch\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mline\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mpos\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    526\u001b[0m             \u001b[1;32mif\u001b[0m \u001b[0mpseudomatch\u001b[0m\u001b[1;33m:\u001b[0m                                \u001b[1;31m# scan for tokens\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m--> 527\u001b[1;33m                 \u001b[0mstart\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mend\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mpseudomatch\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mspan\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m1\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m    528\u001b[0m                 \u001b[0mspos\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mepos\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mpos\u001b[0m \u001b[1;33m=\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0mlnum\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mstart\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0mlnum\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mend\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mend\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    529\u001b[0m                 \u001b[1;32mif\u001b[0m \u001b[0mstart\u001b[0m \u001b[1;33m==\u001b[0m \u001b[0mend\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mKeyboardInterrupt\u001b[0m: "
     ],
     "ename": "KeyboardInterrupt",
     "evalue": "",
     "output_type": "error"
    }
   ],
   "source": [
    "plan = forward_planner( start_state, goal, actions)\n",
    "print(plan)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "plan_with_states = forward_planner( start_state, goal, actions, debug=True)\n",
    "print(plan_with_states)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "102px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "source": [],
    "metadata": {
     "collapsed": false
    }
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
