import csv
from collections import defaultdict
from math import sqrt
from random import shuffle

import matplotlib.pyplot as plt
import numpy as np


def read_csv_file(filename):
    """ Read in the data from the provided file and return as two shuffled sets of data.

    The single character domain specifier is replaced by the full name of the domain in this function.

    Args:
        filename: Provided file name to read from.

    Returns:
        Two shuffled lists of input data from the provided filename.
    """
    rows = []

    # read in the data, replacing the single character domain with the full word
    with open(filename, 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
            rows.append(row)

    # randomize the rows of input
    shuffle(rows)
    shuffle(rows)

    # return two sets of data
    set1, set2 = np.array_split(rows, [int(len(rows) * .67)])
    return set1.tolist(), set2.tolist()


def error_rate(actual, predicted):
    """ Function to print the confusion matrix and error rate for the actual vs predicted

    Args:
        actual: The generated test data of hills and not-hills.
        predicted: The predictions of hills based on our model.
    """
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0
    for i in range(len(actual)):
        if actual[i] == 1 and int(predicted[i]) == 1:
            true_positive += 1
        elif actual[i] == 0 and int(predicted[i]) == 1:
            false_positive += 1
        elif actual[i] == 0 and int(predicted[i]) == 0:
            true_negative += 1
        elif actual[i] == 1 and int(predicted[i]) == 0:
            false_negative += 1
    return (false_negative + false_positive) / len(actual)


def kNN(k, test_item, training_data):
    distances = []
    for data_item in training_data:
        distance = 0.0
        for i in range(0, len(test_item) - 1):
            distance += (float(data_item[i]) - float(test_item[i])) ** 2
        distances.append((sqrt(distance), data_item[len(data_item) - 1]))
    # sort primarily by the distances, and if there are ties (there are, in this dataset), sort by smallest class label
    sorted_vals = sorted(distances, key=lambda x: (x[0], x[1]))
    k_sorted_val = [float(tup[1]) for tup in sorted_vals[:k]]
    return np.mean(k_sorted_val)


def perform_kNN(k, test_data, training_data):
    kNN_error = 0.0
    count = 0
    for set in test_data:
        count += 1
        kNN_estimate = kNN(k, set, training_data)
        error = kNN_estimate - float(set[len(set) - 1])
        if verbose:
            print(f'kNN estimate: {kNN_estimate} actual value: {set[len(set) - 1]}')
        kNN_error += error ** 2  # abs(kNN_value - float(set[len(set) - 1]))
    kNN_error /= count
    return kNN_error, count


# Problem #2
training_data, test_data = read_csv_file('concrete_compressive_strength(1).csv')
kNN_test_errors = defaultdict(lambda: 0.0)
kNN_training_errors = defaultdict(lambda: 0.0)
verbose = False
for k in range(1, 11):
    kNN_test_errors[k], test_count = perform_kNN(k, test_data, training_data)
    kNN_training_errors[k], training_count = perform_kNN(k, training_data, training_data)
    print(f'Test k:{k} score {kNN_test_errors[k]}')
plt.figure(figsize=(10, 7))
plt.plot(list(kNN_training_errors.keys()), list(kNN_training_errors.values()), color='b', label='Training')
plt.plot(list(kNN_test_errors.keys()), list(kNN_test_errors.values()), color='r', label='Test')
plt.xlabel('k Nearest Neighbors')
plt.ylabel('Error Rate')
plt.legend()
plt.show()
# plot error for each k from 1 to 10 against "flat"
print()
