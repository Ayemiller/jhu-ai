import csv
from collections import defaultdict
from math import sqrt
from random import shuffle

import matplotlib.pyplot as plt
import numpy as np


def read_csv_file(filename):
    """ Read in the data from the provided file and return as two shuffled sets of data.

    Args:
        filename: Provided file name to read from.

    Returns:
        Two shuffled lists of input data from the provided filename.
    """
    rows = []

    # read in the data, replacing the single character domain with the full word
    with open(filename, 'r') as csvfile:
        rows = [row for row in csv.reader(csvfile)]

    # randomize the rows of input
    shuffle(rows)
    shuffle(rows)

    # return two sets of data
    set1, set2 = np.array_split(rows, [int(len(rows) * .67)])
    return set1.tolist(), set2.tolist()


def knn(minK, maxK, test_data, training_data):
    """ Function that performs knn as described in this module's pseudocode.

    This function performs kNN on test_data by using the "model" of training_data.
    The function will output a dict that contains the mean squared error for all
    values of k where minK <= k <= maxK.

    Instead of having this function return the values of the nearest neighbors,
    I've chosen to return the mean squared error. By doing this, the same set
    of sorted neighbors can be saved and reused, which greatly speeds up
    kNN on the same data for different k values.

    Args:
        minK: The lower bound of k for kNN to perform.
        maxK: The upper bound of k for kNN to perform.
        test_data: The "test" data for kNN.
        training_data: The "training" data for kNN.

    Returns:
        Dict containing key of the "k" value mapped to its MSE.

    """
    kNN_error = defaultdict(lambda: 0.0)

    for set in test_data:
        distances = []
        for data_item in training_data:
            distance = sum([((float(data_item[i]) - float(set[i])) ** 2) for i in range(0, len(set) - 1)])
            distances.append((sqrt(distance), data_item[len(data_item) - 1]))
        # sort primarily by the distances, and if there are ties (there are, in this dataset)
        # sort by smallest class label
        sorted_vals = sorted(distances, key=lambda x: (x[0], x[1]))
        for i in range(minK, maxK + 1):
            k_nearest = [float(tup[1]) for tup in sorted_vals[:i]]
            kNN_error[i] += (np.mean(k_nearest) - float(set[len(set) - 1])) ** 2

    for i in range(minK, maxK + 1):
        kNN_error[i] /= len(test_data)
    return kNN_error


# Task #2
training_data, test_data = read_csv_file('concrete_compressive_strength(1).csv')
kNN_test_errors = defaultdict(lambda: 0.0)
kNN_training_errors = defaultdict(lambda: 0.0)
verbose = False

kNN_test_errors = knn(1, 10, test_data, training_data)
kNN_training_errors = knn(1, 10, training_data, training_data)

for k in range(1, 11):
    if verbose:
        print(f'Test k:{k} score {kNN_test_errors[k]}')
plt.figure(figsize=(10, 7))
plt.plot(list(kNN_training_errors.keys()), list(kNN_training_errors.values()), color='b', label='Training')
plt.plot(list(kNN_test_errors.keys()), list(kNN_test_errors.values()), color='r', label='Test')
plt.xlabel('k Nearest Neighbors')
plt.ylabel('Mean Squared Error')
plt.legend()
plt.show()

# Task #3
percent = 10
kNN_test_errors = {}
kNN_training_errors = {}
while percent <= 100:
    training_data_subset = training_data[:int(percent * 0.01 * len(training_data))]
    kNN_test_errors[percent] = knn(3, 3, test_data, training_data_subset)[3]
    kNN_training_errors[percent] = knn(3, 3, training_data_subset, training_data_subset)[3]
    if verbose:
        print(f'Test k=3 Percentage of Training Data Used: {percent}% MSE: {kNN_test_errors[percent]}')
    percent += 10
plt.figure(figsize=(10, 7))
plt.plot(list(kNN_training_errors.keys()), list(kNN_training_errors.values()), color='b', label='Training')
plt.plot(list(kNN_test_errors.keys()), list(kNN_test_errors.values()), color='r', label='Test')
plt.xlabel('Percent of Training Data Used')
plt.ylabel('Mean Squared Error')
plt.legend()
plt.show()

# Task #4
data = training_data + test_data
num_chunks = 10
chunks = np.array_split(data, 10)
kNN_test_errors = {}
kNN_training_errors = {}
knn = []
for i in range(0, num_chunks):
    test_chunk = chunks[i]
    training_chunks = np.delete(chunks, i, 0).reshape(int(len(data) - len(data) / num_chunks), len(data[0]))
    knn.append(knn(3, 3, test_chunk, training_chunks)[3])
    if verbose:
        print(f'Test error {i}: {knn[i]}')
print(f'Mean MSE: {np.mean(knn)}')
print(f'MSE Std Dev: {np.std(knn)}')
