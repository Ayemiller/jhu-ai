# %%

import random

import numpy as np

clean_data = {
        "plains": [
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, "plains"]
        ],
        "forest": [
                [0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, "forest"],
                [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, "forest"],
                [1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, "forest"],
                [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, "forest"]
        ],
        "hills": [
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, "hills"],
                [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, "hills"],
                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, "hills"],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, "hills"]
        ],
        "swamp": [
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, "swamp"],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, "swamp"]
        ]
}


def blur(data):
    def apply_noise(value):
        if value < 0.5:
            v = random.gauss(0.10, 0.05)
            if v < 0.0:
                return 0.0
            if v > 0.75:
                return 0.75
            return v
        else:
            v = random.gauss(0.90, 0.10)
            if v < 0.25:
                return 0.25
            if v > 1.00:
                return 1.00
            return v

    noisy_readings = [apply_noise(v) for v in data[0:-1]]
    return noisy_readings + [data[-1]]


def generate_data(clean_data, n, key_label):
    labels = set(clean_data.keys())
    labels.remove(key_label)

    total_per_label = int(n / len(labels))
    data = []
    one_more = True
    for label in labels:

        total_per_label = int(n / len(labels))

        for _ in range(total_per_label):
            if (one_more):
                datum = blur(random.choice(clean_data[label]))
                xs = datum[0:-1]
                data.append((xs, 0))
                one_more = False
            datum = blur(random.choice(clean_data[label]))
            xs = datum[0:-1]
            data.append((xs, 0))

    # create n "label" and code as y=1
    for _ in range(n):
        datum = blur(random.choice(clean_data[key_label]))
        xs = datum[0:-1]
        data.append((xs, 1))
    random.shuffle(data)
    return data


def calc_y_hat(val):
    return 1 / (1 + np.exp(-val))


def calculate_error(y_hat, y):
    return (-y * np.log(y_hat) - (1 - y) * np.log(1 - y_hat)).mean()


def learn_model(data):
    x, y = zip(*data)
    x = list(x)
    y = np.array(list(y))
    x = np.concatenate((np.ones((len(x), 1)), x), axis=1)
    # initialize thetas to 0
    thetas = np.zeros(x.shape[1])

    # for i in range(self.num_iter):
    i = 0
    current_error = 1
    previous_error = 0
    while abs(current_error - previous_error) > 0.0000001:
        y_hat = np.dot(x, thetas)
        y_hat = calc_y_hat(y_hat)
        y_hat = np.dot(x.transpose(), (y_hat - y)) / len(y)
        thetas -= alpha * y_hat

        y_hat = np.dot(x, thetas)
        y_hat = calc_y_hat(y_hat)
        previous_error = current_error
        current_error = calculate_error(y_hat, y)

        if verbose or i % 1000 == 0:
            print(f'Iteration {i} current error={current_error}')
        i += 1
    return thetas


def apply_model(model, test_data):
    test_data = np.concatenate((np.ones((len(test_data), 1)), test_data), axis=1)
    return calc_y_hat(np.dot(test_data, model)).round()


def error_rate(actual, predicted):
    """ Function to print the confusion matrix and error rate for the actual vs predicted

    Args:
        actual: The generated test data of hills and not-hills.
        predicted: The predictions of hills based on our model.
    """
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0
    for i in range(len(y_test)):
        if actual[i] == 1 and int(predicted[i]) == 1:
            true_positive += 1
        elif actual[i] == 0 and int(predicted[i]) == 1:
            false_positive += 1
        elif actual[i] == 0 and int(predicted[i]) == 0:
            true_negative += 1
        elif actual[i] == 1 and int(predicted[i]) == 0:
            false_negative += 1
    print(f'True Positives:{true_positive}')
    print(f'False Positives:{false_positive}')
    print(f'True Negatives:{true_negative}')
    print(f'False Negatives:{false_negative}')
    print(f'Error Rate: {(false_negative + false_positive) / len(y_test)}')


alpha = 0.1
verbose = False

data = generate_data(clean_data, 100, "hills")
model = learn_model(data)
test_data = generate_data(clean_data, 100, "hills")
x_test, y_test = zip(*data)

results = apply_model(model, list(x_test))

# Building the Confusion Matrix
if verbose:
    print(f'preds: {results}')
    print(f'tests: {y_test}')
    print(f'theta: {model}')
error_rate(y_test, results)
