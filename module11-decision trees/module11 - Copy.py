from __future__ import division

import csv
import math
import random
from collections import defaultdict
from copy import deepcopy

import numpy as np


def read_csv_file(filename):
    rows = []
    with open(filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            rows.append(row)

    # randomize the rows of input
    random.shuffle(rows)

    set1, set2 = np.array_split(rows, 2)
    set1 = set1.tolist()
    set2 = set2.tolist()

    return set1, set2


def read_self_check():
    self_check = [['no', 'round', 'large', 'blue'],  # 1
                  ['yes', 'square', 'large', 'green'],
                  ['no', 'square', 'small', 'red'],
                  ['yes', 'round', 'large', 'red'],  # 4
                  ['no', 'square', 'small', 'blue'],
                  ['no', 'round', 'small', 'blue'],
                  ['yes', 'round', 'small', 'red'],
                  ['no', 'square', 'small', 'green'],  # 8
                  ['yes', 'round', 'large', 'green'],
                  ['yes', 'square', 'large', 'green'],
                  ['no', 'square', 'large', 'red'],
                  ['yes', 'square', 'large', 'green'],
                  ['yes', 'round', 'large', 'red'],
                  ['no', 'square', 'small', 'red'],
                  ['no', 'round', 'small', 'green']]
    return self_check


class Node(object):
    def __init__(self, attribute):
        self.attribute = attribute
        self.children = {}

    def __str__(self):
        return self.attribute


def is_homogenous(data):
    """ Helper function that indicates whether or not the provided data is homogeneous.

    Args:
        data: The provided data set.

    Returns:
        Returns true of the data is homogeneous, false otherwise.

    """
    class_label = data[0][0]
    for index in range(0, len(data)):
        if (data[index][0] != class_label):
            return False
    return True


def majority_label(data):
    """ Function that returns the majority label for the provided data.

    Args:
        data: The provided data set

    Returns:
        Returns the string label that accounts for the majority of the provided data set.

    """
    labels = defaultdict(lambda: 0)
    for entry in data:
        # increment count of each entry
        labels[entry[0]] += 1
    max_label = '?'
    max_count = -1
    for key in labels.keys():
        if labels[key] > max_count:
            max_label = key
            max_count = labels[key]
    return max_label


def pick_best_attribute(data, attributes):
    attr_dict = {}

    # loop through each -attribute-
    for attribute_index in range(1, len(attributes)):
        value_dict = {}
        # loop through each sample in dataset
        for sample in data:
            attribute_value = sample[attribute_index]
            class_value = sample[0]
            if not value_dict.get(attribute_value):
                value_dict[attribute_value] = defaultdict(lambda: 0)
            value_dict[attribute_value][class_value] += 1
        attr_dict[attribute_index] = value_dict

    # calc the entropies
    attribute_entropies = {}
    for attribute_index, value_dict in attr_dict.items():
        entropy = 0.0
        total_count = 0
        for attribute, class_dict in value_dict.items():
            individual_entropy = 0.0
            total = sum(class_dict.values())
            total_count += total
            for clazz, count in class_dict.items():
                individual_entropy += -1 * count / total * math.log(count / total, 2) * total
            entropy += individual_entropy
            # print(individual_entropy)
        entropy /= total_count
        # print(entropy)
        attribute_entropies[attribute_index] = entropy
    print(attribute_entropies)
    return attributes[min(attribute_entropies, key=attribute_entropies.get)]


def domain_of(attribute, data, attributes):
    """ Returns the domain of an given attribute

    Args:
        attribute: The attribute who's domain we are searching for
        data: The set of data we're searching
        attributes: The set of attributes in our data

    Returns:
        List containing the domain of a given attribute.

    """
    # domain = set()
    # index = attributes.index(attribute)
    # for entry in data:
    #     domain.add(entry[index])
    # return list(domain)

    domain = defaultdict(lambda: 0)
    index = attributes.index(attribute)
    for entry in data:
        domain[entry[index]] += 1
    # sort the domain by number of each element, greatest to least
    return [i[1] for i in sorted(((value, key) for (key, value) in domain.items()), reverse=True)]


def get_subset_of_data(value, attribute, data, attributes):
    """ Function to return a subset of data that contains a specific value for an attribute.

    Args:
        value: The specified value our subset should have.
        attribute: The specified attribute for the value.
        data: The data set from which we will create a subset.
        attributes: The set of all attributes.

    Returns:
        A subset of only the data which contains a specified value for a specified attribute.

    """
    index = attributes.index(attribute)
    subset = []
    for entry in data:
        if entry[index] == value:
            subset.append(entry)
    subset = deepcopy(subset)
    [row.pop(index) for row in subset]
    return subset


def print_tree(tree, prefix=""):
    """ Prints the decision tree in an ASCII format

    Args:
        tree: the decision tree
        prefix: parameter to help with spacing
    """
    print(f'{prefix}|--{tree}')
    prefix += "|  "
    if isinstance(tree, Node):
        for child_key, child_value in tree.children.items():
            if isinstance(child_key, str):
                print(f'{prefix}|\n{prefix}|{child_key}\n{prefix}|')
            print_tree(child_value, prefix)


def id3(data, attributes, default):
    if not data:
        return default
    if is_homogenous(data):
        return data[0][0]
    if not attributes:
        return majority_label(data)
    best_attribute = pick_best_attribute(data, attributes)
    node = Node(best_attribute)
    default_label = majority_label(data)

    for value in domain_of(best_attribute, data, attributes):
        subset = get_subset_of_data(value, best_attribute, data, attributes)
        recurse_attributes = deepcopy(attributes)
        recurse_attributes.remove(best_attribute)
        child = id3(subset, recurse_attributes, default_label)
        node.children[value] = child

    return node


# split into two sets

# *****************************************************************************************
# required functions below
# *****************************************************************************************

field_names = ['edible', 'cap-shape', 'cap-surface', 'cap-color', 'bruises', 'odor', 'gill-attachment',
               'gill-spacing',
               'gill-size', 'gill-color', 'stalk-shape', 'stalk-root', 'stalk-surface-above-ring',
               'stalk-surface-below-ring', 'stalk-color-above-ring', 'stalk-color-below-ring', 'veil-type',
               'veil-color', 'ring-number', 'ring-type', 'spore-print-color', 'population', 'habitat']


# field_names = ['safe', 'shape', 'size', 'color']


def train(training_data):
    """ Creates a decision tree for the provided training data using the ID3 algorithm.

    Args:
        training_data: The provided training data. The "class" is expected at the first index of each row.

    Returns:
        Decision tree created from ID3 algorithm for the provided data.
    """
    # returns a decision tree data structure

    return id3(training_data, field_names, 'yes')


def view(tree):
    """ Prints the decision tree.

    Args:
        tree: the decision tree
    """
    print_tree(tree)


def recursively_classify(tree, item):
    # find the root of the tree in item, call child we're looking for

    if isinstance(tree, Node):
        item_to_remove = item[field_names.index(tree.attribute)]
        newtree = tree.children[item_to_remove]
        return recursively_classify(newtree, item)
    else:
        return tree


def classify(tree, test_data):
    # returns a list of classifications
    classifications = []
    for item in test_data:
        classifications.append(recursively_classify(tree, item))

    return classifications


def evaluate(test_data, classifications):
    test_classes = [row[0] for row in test_data]
    errors = 0
    for item1, item2 in zip(test_classes, classifications):
        if item1 != item2:
            errors += 1
    return errors / len(test_data)


set1, set2 = read_csv_file('agaricus-lepiota.data')
# set1 = read_self_check()
# set2 = read_self_check()
decision_tree = train(set1)
view(decision_tree)
classifications = classify(decision_tree, set2)
error_rate = evaluate(set1, classifications)
print(f'Error rate: {error_rate}')
