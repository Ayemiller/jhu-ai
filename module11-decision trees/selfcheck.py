import numpy as np
from id3 import Id3Estimator, export_graphviz, export_text

feature_names = ["age",
                 "gender",
                 "sector",
                 "degree"]

X = np.array([[45, "male", "private", "m"],
              [50, "female", "private", "m"],
              [61, "other", "public", "b"],
              [40, "male", "private", "none"],
              [34, "female", "private", "none"],
              [33, "male", "public", "none"],
              [43, "other", "private", "m"],
              [35, "male", "private", "m"],
              [34, "female", "private", "m"],
              [35, "male", "public", "m"],
              [34, "other", "public", "m"],
              [34, "other", "public", "b"],
              [34, "female", "public", "b"],
              [34, "male", "public", "b"],
              [34, "female", "private", "b"],
              [34, "male", "private", "b"],
              [34, "other", "private", "b"]])

y = np.array(["(30,38)",
              "(30,38)",
              "(30,38)",
              "(13,15)",
              "(13,15)",
              "(13,15)",
              "(23,30)",
              "(23,30)",
              "(23,30)",
              "(15,23)",
              "(15,23)",
              "(15,23)",
              "(15,23)",
              "(15,23)",
              "(23,30)",
              "(23,30)",
              "(23,30)"])

clf = Id3Estimator()
clf.fit(X, y, check_input=True)

export_graphviz(clf.tree_, "out.dot", feature_names)
print(export_text(clf.tree_, feature_names))
