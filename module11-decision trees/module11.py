from __future__ import division

import csv
import math
import random
from collections import defaultdict
from copy import deepcopy

import numpy as np


def read_csv_file(filename):
    """ Read in the data from the provided file and return as two shuffled sets of data.

    The single character domain specifier is replaced by the full name of the domain in this function.

    Args:
        filename: Provided file name to read from.

    Returns:
        Two shuffled lists of input data from the provided filename.
    """
    rows = []
    field_domains = [
            {'p': 'poisonous', 'e': 'edible'},
            {'b': 'bell', 'c': 'conical', 'x': 'convex', 'f': 'flat', 'k': 'knobbed', 's': 'sunken'},
            {'f': 'fibrous', 'g': 'grooves', 'y': 'scaly', 's': 'smooth'},
            {'n': 'brown', 'b': 'buff', 'c': 'cinnamon', 'g': 'gray', 'r': 'green', 'p': 'pink', 'u': 'purple',
             'e': 'red',
             'w': 'white', 'y': 'yellow'},
            {'t': 'bruises', 'f': 'no'},
            {'a': 'almond', 'l': 'anise', 'c': 'creosote', 'y': 'fishy', 'f': 'foul', 'm': 'musty', 'n': 'none',
             'p': 'pungent', 's': 'spicy'},
            {'a': 'attached', 'd': 'descending', 'f': 'free', 'n': 'notched'},
            {'c': 'close', 'w': 'crowded', 'd': 'distant'},
            {'b': 'broad', 'n': 'narrow'},
            {'k': 'black', 'n': 'brown', 'b': 'buff', 'h': 'chocolate', 'g': 'gray', 'r': 'green', 'o': 'orange',
             'p': 'pink', 'u': 'purple', 'e': 'red', 'w': 'white', 'y': 'yellow'},
            {'e': 'enlarging', 't': 'tapering'},
            {'b': 'bulbous', 'c': 'club', 'u': 'cup', 'e': 'equal', 'z': 'rhizomorphs', 'r': 'rooted', '?': 'missing'},
            {'f': 'fibrous', 'y': 'scaly', 'k': 'silky', 's': 'smooth'},
            {'f': 'fibrous', 'y': 'scaly', 'k': 'silky', 's': 'smooth'},
            {'n': 'brown', 'b': 'buff', 'c': 'cinnamon', 'g': 'gray', 'o': 'orange', 'p': 'pink', 'e': 'red',
             'w': 'white',
             'y': 'yellow'},
            {'n': 'brown', 'b': 'buff', 'c': 'cinnamon', 'g': 'gray', 'o': 'orange', 'p': 'pink', 'e': 'red',
             'w': 'white',
             'y': 'yellow'},
            {'p': 'partial', 'u': 'universal'},
            {'n': 'brown', 'o': 'orange', 'w': 'white', 'y': 'yellow'},
            {'n': 'none', 'o': 'one', 't': 'two'},
            {'c': 'cobwebby', 'e': 'evanescent', 'f': 'flaring', 'l': 'large', 'n': 'none', 'p': 'pendant',
             's': 'sheathing', 'z': 'zone'},
            {'k': 'black', 'n': 'brown', 'b': 'buff', 'h': 'chocolate', 'r': 'green', 'o': 'orange', 'u': 'purple',
             'w': 'white', 'y': 'yellow'},
            {'a': 'abundant', 'c': 'clustered', 'n': 'numerous', 's': 'scattered', 'v': 'several', 'y': 'solitary'},
            {'g': 'grasses', 'l': 'leaves', 'm': 'meadows', 'p': 'paths', 'u': 'urban', 'w': 'waste', 'd': 'woods'},
    ]

    # read in the data, replacing the single character domain with the full word
    with open(filename, 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
            new_row = []
            for i in range(0, len(row)):
                new_row.append(field_domains[i].get(row[i]))
            rows.append(new_row)

    # randomize the rows of input
    random.shuffle(rows)

    # return two sets of data
    set1, set2 = np.array_split(rows, 2)
    return set1.tolist(), set2.tolist()


def read_self_check():
    self_check = [['no', 'round', 'large', 'blue'],
                  ['yes', 'square', 'large', 'green'],
                  ['no', 'square', 'small', 'red'],
                  ['yes', 'round', 'large', 'red'],
                  ['no', 'square', 'small', 'blue'],
                  ['no', 'round', 'small', 'blue'],
                  ['yes', 'round', 'small', 'red'],
                  ['no', 'square', 'small', 'green'],
                  ['yes', 'round', 'large', 'green'],
                  ['yes', 'square', 'large', 'green'],
                  ['no', 'square', 'large', 'red'],
                  ['yes', 'square', 'large', 'green'],
                  ['yes', 'round', 'large', 'red'],
                  ['no', 'square', 'small', 'red'],
                  ['no', 'round', 'small', 'green']]
    return self_check


class Node(object):
    """ Class to represent nodes for our decision tree.

    Attributes:
        attribute (str): Description of this node
        children (:obj:`dict` of str or :obj:`Node`): Children of this node

    """

    def __init__(self, attribute):
        self.attribute = attribute
        self.children = {}

    def __str__(self):
        return self.attribute


def is_homogenous(data):
    """ Helper function that indicates whether or not the provided data is homogeneous.

    Args:
        data: The provided data set.

    Returns:
        Returns true of the data is homogeneous, false otherwise.

    """
    class_label = data[0][0]
    for index in range(0, len(data)):
        if (data[index][0] != class_label):
            return False
    return True


def majority_label(data):
    """ Function that returns the majority label for the provided data.

    Args:
        data: The provided data set

    Returns:
        Returns the string label that accounts for the majority of the provided data set.

    """
    labels = defaultdict(lambda: 0)
    for entry in data:
        # increment count of each entry
        labels[entry[0]] += 1
    max_label = '?'
    max_count = -1
    for key in labels.keys():
        if labels[key] > max_count:
            max_label = key
            max_count = labels[key]
    return max_label


def pick_best_attribute(data, attributes):
    """ This function chooses the best attribute based on 'information gain' exercise from self-check

    Args:
        data: The provided data
        attributes: The provided attributes

    Returns:
        Returns the name of the attribute with the highest information gain.

    """
    attr_dict = {}

    # loop through each -attribute-
    for attribute_index in range(1, len(attributes)):
        value_dict = {}
        # loop through each sample in dataset
        for sample in data:
            attribute_value = sample[attribute_index]
            class_value = sample[0]
            if not value_dict.get(attribute_value):
                value_dict[attribute_value] = defaultdict(lambda: 0)
            value_dict[attribute_value][class_value] += 1
        attr_dict[attribute_index] = value_dict

    # calc the entropies
    attribute_entropies = {}
    for attribute_index, value_dict in attr_dict.items():
        entropy = 0.0
        total_count = 0
        for attribute, class_dict in value_dict.items():
            individual_entropy = 0.0
            total = sum(class_dict.values())
            total_count += total
            for clazz, count in class_dict.items():
                individual_entropy += -1 * count / total * math.log(count / total, 2) * total
            entropy += individual_entropy
        entropy /= total_count
        attribute_entropies[attribute_index] = entropy
    if verbose:
        print(attribute_entropies)

    # return the attribute that provides the highest information gain
    return attributes[min(attribute_entropies, key=attribute_entropies.get)]


def domain_of(attribute, data, attributes):
    """ Returns the domain of an given attribute

    Args:
        attribute: The attribute who's domain we are searching for
        data: The set of data we're searching
        attributes: The set of attributes in our data

    Returns:
        List containing the domain of a given attribute.

    """
    domain = defaultdict(lambda: 0)
    index = attributes.index(attribute)
    for entry in data:
        domain[entry[index]] += 1
    # sort the domain by number of each element, greatest to least
    return [i[1] for i in sorted(((value, key) for (key, value) in domain.items()), reverse=True)]


def get_subset_of_data(value, attribute, data, attributes):
    """ Function to return a subset of data that contains a specific value for an attribute.

    Args:
        value: The specified value our subset should have.
        attribute: The specified attribute for the value.
        data: The data set from which we will create a subset.
        attributes: The set of all attributes.

    Returns:
        A subset of only the data which contains a specified value for a specified attribute.

    """
    index = attributes.index(attribute)
    subset = []
    for entry in data:
        if entry[index] == value:
            subset.append(entry)
    subset = deepcopy(subset)
    [row.pop(index) for row in subset]
    return subset


def print_tree(tree, prefix=""):
    """ Prints the decision tree in an ASCII format

    Args:
        tree: the decision tree
        prefix: parameter to help with spacing
    """
    print(f'{prefix}|--{tree}')
    prefix += "|  "
    if isinstance(tree, Node):
        for child_key, child_value in tree.children.items():
            if isinstance(child_key, str):
                print(f'{prefix}|\n{prefix}|{child_key}\n{prefix}|')
            print_tree(child_value, prefix)


def id3(data, attributes, default):
    """ Function that implements the ID3 algorithm as presented in the provided pseudocode.

    Returns:
        Returns the classification tree that is generated from the provided data.
    """
    if not data:
        return default
    if is_homogenous(data):
        return data[0][0]
    if not attributes:
        return majority_label(data)
    best_attribute = pick_best_attribute(data, attributes)
    node = Node(best_attribute)
    default_label = majority_label(data)

    for value in domain_of(best_attribute, data, attributes):
        subset = get_subset_of_data(value, best_attribute, data, attributes)
        recurse_attributes = deepcopy(attributes)
        recurse_attributes.remove(best_attribute)
        child = id3(subset, recurse_attributes, default_label)
        node.children[value] = child

    return node


# *****************************************************************************************
# required functions below
# *****************************************************************************************

field_names = ['edible', 'cap-shape', 'cap-surface', 'cap-color', 'bruises', 'odor', 'gill-attachment',
               'gill-spacing',
               'gill-size', 'gill-color', 'stalk-shape', 'stalk-root', 'stalk-surface-above-ring',
               'stalk-surface-below-ring', 'stalk-color-above-ring', 'stalk-color-below-ring', 'veil-type',
               'veil-color', 'ring-number', 'ring-type', 'spore-print-color', 'population', 'habitat']


def train(training_data):
    """ Creates a decision tree for the provided training data using the ID3 algorithm.

    Args:
        training_data: The provided training data. The "class" is expected at the first index of each row.

    Returns:
        Decision tree created from ID3 algorithm for the provided data.
    """
    # returns a decision tree data structure

    return id3(training_data, field_names, 'yes')


def view(tree):
    """ Prints the decision tree.

    Args:
        tree: the decision tree
    """
    print_tree(tree)


def recursively_classify(tree, item):
    """ Traverse the tree using the provided input item until we reach a leaf.

    Args:
        tree: The provided decision tree.
        item: The provided input data.

    Returns:
        The classification for the input item, either 'edible' or 'poisonous'.
    """

    if isinstance(tree, Node):
        item_to_remove = item[field_names.index(tree.attribute)]

        try:
            newtree = tree.children[item_to_remove]
        except KeyError:
            # Occasionally, I would see a tree that did not contain a permutation in the other, resulting in a KeyError.
            # When this happens, I will classify it as 'poisonous'
            return 'poisonous'

        return recursively_classify(newtree, item)
    else:
        return tree


def classify(tree, test_data):
    """ Returns a list of classifications for the provided data using the provided decision tree.

    Args:
        tree: The provided decision tree.
        test_data: The provided test data.

    Returns:
        A list of strings containing the classifications for a test set against a decision tree.
    """
    classifications = []
    for item in test_data:
        classifications.append(recursively_classify(tree, item))

    return classifications


def evaluate(test_data, classifications):
    """ This function compares the test_data classifications to the created classifications and returns the error-rate.

    Args:
        test_data: Test data from agaricus-lepiota.data
        classifications: Generated classifications from our decision tree.

    Returns:

    """
    test_classes = [row[0] for row in test_data]
    errors = 0
    for item1, item2 in zip(test_classes, classifications):
        if item1 != item2:
            errors += 1
    return errors / len(test_data)


verbose = False

set1, set2 = read_csv_file('agaricus-lepiota.data')
set1_self = read_self_check()
set2_self = read_self_check()
decision_tree_from_set_1 = train(set1)
print('Decision Tree 1')
view(decision_tree_from_set_1)
classifications = classify(decision_tree_from_set_1, set2)
error_rate_1 = evaluate(set2, classifications)
if verbose:
    print(f'Error rate: {error_rate_1}')

decision_tree_from_set_2 = train(set2)
print('\nDecision Tree 2')
view(decision_tree_from_set_2)
classifications = classify(decision_tree_from_set_2, set1)
error_rate_2 = evaluate(set1, classifications)
if verbose:
    print(f'Error rate: {error_rate_2}')

print(f'Average Error Rate: {(error_rate_1 + error_rate_2) / 2}')
