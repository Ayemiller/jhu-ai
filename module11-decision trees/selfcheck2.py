import numpy as np
from id3 import Id3Estimator, export_graphviz, export_text

feature_names = ["shape",
                 "size",
                 "color"]

X = np.array([['round', 'large', 'blue'],  # 1
              ['square', 'large', 'green'],
              ['square', 'small', 'red'],
              ['round', 'large', 'red'],  # 4
              ['square', 'small', 'blue'],
              ['round', 'small', 'blue'],
              ['round', 'small', 'red'],
              ['square', 'small', 'green'],  # 8
              ['round', 'large', 'green'],
              ['square', 'large', 'green'],
              ['square', 'large', 'red'],
              ['square', 'large', 'green'],
              ['round', 'large', 'red'],
              ['square', 'small', 'red'],
              ['round', 'small', 'green']

              ])

y = np.array(['no', 'yes', 'no', 'yes', 'no',
              'no', 'yes', 'no', 'yes', 'yes',
              'no', 'yes', 'yes', 'no', 'no'
              ])

clf = Id3Estimator()
clf.fit(X, y, check_input=True)

export_graphviz(clf.tree_, "out.dot", feature_names)
print(export_text(clf.tree_, feature_names))
