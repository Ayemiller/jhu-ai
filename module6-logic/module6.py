import sys
import tokenize

from io import StringIO
from typing import Union, List, Dict


def atom(next, token):
    """ Function provided with assignment.

    Args:
        next:
        token:

    Returns:

    """
    if token[1] == '(':
        out = []
        token = next()
        while token[1] != ')':
            out.append(atom(next, token))
            token = next()
            if token[1] == ' ':
                token = next()
        return out
    elif token[1] == '?':
        token = next()
        return "?" + token[1]
    else:
        return token[1]


def parse(exp):
    """ Function provided with assignment.

    Args:
        exp:

    Returns:

    """
    src = StringIO(exp).readline
    tokens = tokenize.generate_tokens(src)
    return atom(tokens.__next__, tokens.__next__())


def is_variable(exp):
    """ Function provided with assignment.

    Args:
        exp:

    Returns:
        Returns True if the expression is a variable, False otherwise.

    """
    return isinstance(exp, str) and exp[0] == "?"


def is_constant(exp):
    """ Function provided with assignment.

    Args:
        exp:

    Returns:
        Returns True if the expression is a constant, False otherwise.
    """
    return isinstance(exp, str) and not is_variable(exp)


def apply_result(result: Union[Dict[str, str], Dict[str, List[str]], Dict[str, List[List[str]]]],
                 expression: Union[str, List[str], List[List[str]]]):
    """ Helper function that recursively applies a result mapping to an expression.

    If the result is empty, the function simply returns the expression that was provided.

    If the expression is a list, the function calls itself again on the sub-expression
    so that a replacement can be made.

    Args:
        result: The result mapping a variable to a value.
        expression: An expression that will be updated according to the result mapping provided.

    Returns:
        The updated expression that replaces any variables provided in the result mapping.

    """
    if result == {}:
        return expression
    if isinstance(expression, list):
        return [apply_result(result, sub_expression) for sub_expression in expression]
    else:
        for k, v in result.items():
            if expression == k:
                return v
            else:
                return expression


def unification(exp1: Union[str, List[str], List[List[str]]], exp2: Union[str, List[str], List[List[str]]]) -> Union[
    None, Dict[str, str], Dict[str, List[str]], Dict[str, List[List[str]]]]:
    """ Function to perform a unification of two provided expressions.

    Args:
        exp1: The first expression.
        exp2: The second expression.

    Returns:
        A dictionary containing the substitution list if a valid unification exists, otherwise, None.
    """

    # first conditional in the provided pseudo-code
    if (is_constant(exp1) and is_constant(exp2)) or exp1 == [] == exp2:
        if exp1 == exp2:
            return {}
        else:
            return None
    # second conditional in the provided pseudo-code
    if is_variable(exp1):
        if exp1 in exp2:
            return None
        else:
            return {exp1: exp2}

    # third conditional in the provided pseudo-code
    if is_variable(exp2):
        if exp2 in exp1:
            return None
        else:
            return {exp2: exp1}

    # inductive step in the provided pseudo-code
    if not exp1:
        first1 = []
    else:
        first1 = exp1[0]
    if not exp2:
        first2 = []
    else:
        first2 = exp2[0]

    result1 = unification(first1, first2)
    if result1 is None:
        return None

    # create variables for the "rest of" each expression
    rest_of_exp1 = exp1[1:]
    rest_of_exp2 = exp2[1:]

    # apply result1 to the rest of exp1 and exp2
    rest_of_exp1 = apply_result(result1, rest_of_exp1)
    rest_of_exp2 = apply_result(result1, rest_of_exp2)

    result2 = unification(rest_of_exp1, rest_of_exp2)
    if result2 is None:
        return None

    return {**result1, **result2}


def unify(s_expression1: str, s_expression2: str) -> Union[
    None, Dict[str, str], Dict[str, List[str]], Dict[str, List[List[str]]]]:
    """ Function provided with assignment to call unification on parsed expressions.

    Args:
        s_expression1: The first expression.
        s_expression2: The second expression.

    Returns:
        A dictionary containing the substitution list if a valid unification exists, otherwise, None.
    """
    return unification(parse(s_expression1), parse(s_expression2))


def assert_unify(exp1: Union[str, List[str], List[List[str]]], exp2: Union[str, List[str], List[List[str]]],
                 trg: Union[None, Dict[str, str], Dict[str, List[str]], Dict[str, List[List[str]]]],
                 debug: bool):
    # never do this for real code!
    global unifications
    result: Union[None, Dict[str, str], Dict[str, List[str]], Dict[str, List[List[str]]]] = unify(exp1, exp2)
    if debug:
        print(unifications, exp1, exp2, result, trg)
    assert result == trg
    unifications += 1


def test_unify(debug):
    """ Function provided with assignment to perform tests of the unify function.

    The examples from the self check were added, along with 5 additional tests.

    Args:
        debug: Flag to print out extra information when True.

    """
    # self-check examples
    assert_unify('Fred', 'Barney', None, debug)
    assert_unify('Pebbles', 'Pebbles', {}, debug)
    assert_unify('(quarry_worker Fred)', '(quarry_worker ?x)', {"?x": 'Fred'}, debug)
    assert_unify('(son Barney ?x)', '(son ?y Bam_Bam)', {"?x": 'Bam_Bam', '?y': 'Barney'}, debug)
    assert_unify('(married ?x ?y)', '(married Barney Wilma)', {'?x': 'Barney', '?y': 'Wilma'}, debug)
    assert_unify('(son Barney ?x)', '(son ?y (son Barney))', {'?x': ['son', 'Barney'], '?y': 'Barney'}, debug)
    assert_unify('(son Barney ?x)', '(son ?y (son ?y))', {'?x': ['son', 'Barney'], '?y': 'Barney'}, debug)
    assert_unify('(son Barney Bam_Bam)', '(son ?y (son Barney))', None, debug)
    assert_unify('(loves Fred Fred)', '(loves ?x ?x)', {'?x': 'Fred'}, debug)
    assert_unify('(future George Fred)', '(future ?y ?y)', None, debug)

    # add 5 additional test cases.
    assert_unify('(bacon is ?x)', '(?y is tasty)', {'?x': 'tasty', '?y': 'bacon'}, debug)
    assert_unify('(bacon is ?x)', '(?y is (tasty and fatty))', {'?x': ['tasty', 'and', 'fatty'], '?y': 'bacon'}, debug)
    assert_unify('(?z bacon is ?x)', '(Canadian ?y is (tasty and lean))',
                 {'?x': ['tasty', 'and', 'lean'], '?y': 'bacon', '?z': 'Canadian'}, debug)
    assert_unify('(?w ?z bacon is ?x)', '(uncured Canadian ?y is (tasty and lean))',
                 {'?w': 'uncured', '?x': ['tasty', 'and', 'lean'], '?y': 'bacon', '?z': 'Canadian'}, debug)
    assert_unify('((my favorite) ?w ?z bacon is ?x)', '(?v uncured Canadian ?y is (tasty and lean))',
                 {'?v': ['my', 'favorite'], '?w': 'uncured', '?x': ['tasty', 'and', 'lean'], '?y': 'bacon',
                  '?z': 'Canadian'}, debug)


if __name__ == "__main__":
    # use this debug flag if necessary; otherwise, the program should print out only
    # what is requested.
    global unifications
    unifications = 0
    debug = len(sys.argv) > 1 and sys.argv[1].lower() == 'debug'
    test_unify(debug)
