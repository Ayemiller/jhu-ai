import sys
import time
from collections import OrderedDict
from copy import deepcopy
from typing import Dict, Tuple, List, TypedDict

import matplotlib.pyplot as plt
import networkx as nx


class PlanarMap(TypedDict):
    nodes: List[str]
    edges: List[Tuple[int, int]]
    coordinates: List[Tuple[int, int]]
    assignment: Dict[Tuple[int, str], str]  # dict to keep track of assignments
    edges_by_node: Dict[Tuple[int, str], List[Tuple[Tuple[int, str], Tuple[int, str]]]]  # dict to provide neighbors
    colors_by_node: Dict[Tuple[int, str], List[str]]  # dict to track colors remaining of each node (domain)


def is_consistent(unassigned_node, planar_map: PlanarMap, color):
    """ Helper function to "decide" if a particular assignment is consistent.

    In our world, an assignment is consistent so long as adjacent nodes are not
    the same color. Therefore this function will immediately return false when
    it finds an adjacent node of the unassigned_node that has the provided color.

    Otherwise, it returns true.

    Args:
        unassigned_node: the node that we are testing for assignment
        planar_map: the current state of our world
        color: the color that we are testing for consistency

    Returns:
        true if the color can be assigned consistently, false otherwise
    """
    # the assignment is consistent if no adjacent nodes have the same color

    assignment_map = planar_map["assignment"]
    edges_by_node = planar_map["edges_by_node"]
    consistent = True
    for adjacent_nodes in edges_by_node[unassigned_node]:
        for adjacent_node in adjacent_nodes:
            if adjacent_node == unassigned_node:
                continue
            elif adjacent_node in assignment_map and assignment_map[adjacent_node] == color:
                return False
    return consistent


def get_least_constraining_order(planar_map, current_node: Tuple[int, str]) -> List[str]:
    """ Function to generate a sorted list of neighbor's constraints by color.

    First we find the neighbors of the current node that haven't been assigned.

    Then, for each color that is available to the current node, we count how
    many neighbors have that color available.

    Finally, we sort the colors by the counts we found, from low to high.

    Args:
        planar_map: the current state of our world
        current_node: the node whose neighbors we are looking for

    Returns:
        Sorted list of colors sorted by the number of neighbors who share that color.

    """
    # these are the colors available to the current node
    colors_for_current_node = planar_map["colors_by_node"][current_node]

    # dict to hold count of constraints from neighbors, initialized to zero
    color_counts = {key: 0 for key in colors_for_current_node}

    # "score" the constraints of each color by counting up totals
    for color in colors_for_current_node:

        # iterate through each neighbor of current_node
        for neighbor in get_adjacent_nodes_not_yet_assigned(planar_map, current_node):

            # if the neighbor still has the color we are looking for, increment its count
            if color in planar_map["colors_by_node"][neighbor]:
                color_counts[color] += 1

    # return list of colors sorted from fewest counts to most
    return sorted(color_counts, key=color_counts.get)


def color_map(planar_map, colors: List[str], trace=False):
    """ This function takes the planar_map and tries to assign colors to it.

    planar_map: Dict with keys "nodes", "edges", and "coordinates". "nodes" is a List of node names, "edges"
    is a List of Tuples. Each tuple is a pair of indices into "nodes" that describes an edge between those
    nodes. "coordinates" are x,y coordinates for drawing.

    colors: a List of color names such as ["yellow", "blue", "green"] or ["orange", "red", "yellow", "green"]
    these should be color names recognized by Matplotlib.

    I have added additional keys to planar_map to eliminate the need to re-calculate data that is needed
    throughout the program. The additional keys include "assignment", "edges_by_node", and "colors_by_node".

    "assignment" is a dict containing nodes mapped to a color. "edges_by_node" is a dict containing nodes
    mapped to that node's edges. "colors_by_node" is a dict that contains nodes mapped to the current state
    of that node's color domain. All of the nodes in these additional dicts are represented as a Tuple
    containing the node's index and name. This was done to make it easier to identify the nodes and their edges.
    This is all defined explicitly at the top of this file in the "PlanarMap" TypedDict class.

    This function follows the general definition for backtracking search from the textbook on page 215.
    Using the various cities/countries as our variables, the 3 or 4 different colors as the domain,
    and variables which share an edge as our constraint. I've implemented backtracking, forward-checking,
    minimum remaining values, and least constraining values and as far as I can tell it is all working.
    I recorded some run-times of the 4 problems that we were provided against various combinations of the
    algorithm optimizations as listed below:
        5.2 seconds with backtracking & forward-checking
        4.9 seconds with MRV and no LCV
        5.2 seconds with LCV and no MRV
        4.8 seconds with LCV & MRV

    Returns:
        If a coloring cannot be found, the function returns None. Otherwise, it returns an ordered list of Tuples,
        (node name, color name), with the same order as "nodes".

    """

    # if assignment is complete, then return assignment
    if planar_map["assignment"] is None:
        planar_map["assignment"] = {}

    # make a dict of index mapped to node name
    node_dict: Dict[int, str] = dict(enumerate(planar_map["nodes"]))

    # create a list of un-assigned nodes
    unassigned_nodes: List[Tuple[int, str]] = [node for node in node_dict.items() if
                                               node not in planar_map["assignment"]]

    # if all nodes are assigned, we're done searching, so return the assignment dict
    if not unassigned_nodes:
        return simplify_map(planar_map["assignment"])

    # perform Minimum Remaining Values here
    unassigned_nodes_with_values: Dict[Tuple[int, str], List[str]] = create_minimum_remaining_values(unassigned_nodes,
                                                                                                     planar_map)

    # sort all of the unassigned nodes by the length of there remaining values list
    # pick the node that has the fewest entries in planar_map["colors_by_node"]
    current_node: Tuple[int, str] = sorted(unassigned_nodes_with_values.items(), key=lambda x: len(x[1]))[0][0]

    # setting current_node using the statement below would "disable" MRV
    # ------
    # DISABLE MRV
    # current_node = unassigned_nodes[0]
    # ------
    if trace:
        print(
            f'MRV- chose {current_node[1]} because it has '
            f'{len(unassigned_nodes_with_values[current_node])} remaining values.')

    # Perform Least Constraining Value here
    # Pick the color based on the selection that would rule out the fewest choices for the neighboring variables
    lcv_ordered_colors: List[str] = get_least_constraining_order(planar_map, current_node)
    if trace and len(lcv_ordered_colors) != 0:
        print(
            f'LCV- chose {lcv_ordered_colors[0]} for {current_node[1]} '
            f'because it appears the fewest times in neighbor\'s domains.')
    # Using the commented-out for-loop below instead would "disable" LCV
    # ------
    # DISABLE LCV
    # for color in planar_map["colors_by_node"][current_node]:
    # ------
    for color in lcv_ordered_colors:

        # Make a copy of planar_map so any changes we make as we recurse don't effect us when we backtrack
        copy_planar_map = deepcopy(planar_map)

        # Assign the color to the current node (which will also wipe out the old, wrong assignments).
        copy_planar_map["assignment"][current_node] = color

        # check to see if our assignment is consistent
        if is_consistent(current_node, copy_planar_map, color):

            # Perform forward-checking and remove the assigned color from adjacent nodes
            # To disable forward-checking, comment out the line below.
            perform_forward_checking(color, copy_planar_map, current_node, trace)

            # Make recursive call to this function, now searching our copied planar map for backtracking
            result = color_map(copy_planar_map, colors, trace)

            # Returning the correct coloring back to the first recursive call.
            if result is not None:
                return result

    # If we make it here, we are effectively backtracking. Return none will give
    # control back to the recursive caller, and we'll continue looking for a
    # consistent assignment.
    if trace:
        print(f'Backtracking- Unable to find a consistent assignment for {current_node}, backtracking...')
    return None


def perform_forward_checking(color: str, planar_map: PlanarMap, current_node: Tuple[int, str], trace: bool):
    """ Function that performs forward checking.

    If the currently assigned color exists in any neighboring, non-assigned, nodes, remove it!

    Args:
        color: the assigned color
        planar_map: the current state of our world
        current_node: the current node we just assigned
        trace: flag to enable debug print messages
    """
    for adjacent_node in get_adjacent_nodes_not_yet_assigned(planar_map, current_node):
        if color in planar_map["colors_by_node"][adjacent_node]:
            if trace:
                print(f'Forward Checking- removing the color {color} from {adjacent_node[1]} '
                      f'because it was assigned to {current_node[1]}')
            planar_map["colors_by_node"][adjacent_node].remove(color)


def get_adjacent_nodes_not_yet_assigned(planar_map: PlanarMap, current_node: Tuple[int, str]) -> List[Tuple[int, str]]:
    """ Helper function to return the neighbors of a given node that have not yet been assigned.

    Args:
        planar_map: the current state of our world
        current_node: the current node we just assigned

    Returns:
        A list of un-assigned neighbor nodes.

    """
    # get de-duplicated list of neighbors of current node from our edges dict
    adjacent_nodes = list(
        OrderedDict.fromkeys([node for sublist in planar_map["edges_by_node"][current_node] for node in sublist]))
    # make sure that current_node and already assigned nodes are removed
    adjacent_nodes = [neighbor for neighbor in adjacent_nodes if
                      neighbor not in planar_map["assignment"].keys() and neighbor != current_node]
    return adjacent_nodes


def create_minimum_remaining_values(unassigned_nodes: List[Tuple[int, str]], planar_map: PlanarMap) \
        -> Dict[Tuple[int, str], List[str]]:
    """ This function creates a dictionary of nodes mapped to their remaining colors.

    Nodes that have already been assigned are not included in this list.

    Args:
        unassigned_nodes: a list of unassigned nodes
        planar_map: the current state of our world

    Returns:
        A dict containing un-assigned nodes mapped to the remaining colors in their domain.

    """
    return {node: planar_map["colors_by_node"][node] for node in unassigned_nodes}


connecticut = {
        "nodes": ["Fairfield", "Litchfield", "New Haven", "Hartford", "Middlesex", "Tolland", "New London", "Windham"],
        "edges": [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (2, 4), (3, 4), (3, 5), (3, 6), (4, 6), (5, 6), (5, 7),
                  (6, 7)],
        "coordinates": [(46, 52), (65, 142), (104, 77), (123, 142), (147, 85), (162, 140), (197, 94), (217, 146)]}

europe = {
        "nodes": ["Iceland", "Ireland", "United Kingdom", "Portugal", "Spain",
                  "France", "Belgium", "Netherlands", "Luxembourg", "Germany",
                  "Denmark", "Norway", "Sweden", "Finland", "Estonia",
                  "Latvia", "Lithuania", "Poland", "Czech Republic", "Austria",
                  "Liechtenstein", "Switzerland", "Italy", "Malta", "Greece",
                  "Albania", "Macedonia", "Kosovo", "Montenegro", "Bosnia Herzegovina",
                  "Serbia", "Croatia", "Slovenia", "Hungary", "Slovakia",
                  "Belarus", "Ukraine", "Moldova", "Romania", "Bulgaria",
                  "Cyprus", "Turkey", "Georgia", "Armenia", "Azerbaijan",
                  "Russia"],
        "edges": [(0, 1), (0, 2), (1, 2), (2, 5), (2, 6), (2, 7), (2, 11), (3, 4),
                  (4, 5), (4, 22), (5, 6), (5, 8), (5, 9), (5, 21), (5, 22), (6, 7),
                  (6, 8), (6, 9), (7, 9), (8, 9), (9, 10), (9, 12), (9, 17), (9, 18),
                  (9, 19), (9, 21), (10, 11), (10, 12), (10, 17), (11, 12), (11, 13), (11, 45),
                  (12, 13), (12, 14), (12, 15), (12, 17), (13, 14), (13, 45), (14, 15),
                  (14, 45), (15, 16), (15, 35), (15, 45), (16, 17), (16, 35), (17, 18),
                  (17, 34), (17, 35), (17, 36), (18, 19), (18, 34), (19, 20), (19, 21),
                  (19, 22), (19, 32), (19, 33), (19, 34), (20, 21), (21, 22), (22, 23),
                  (22, 24), (22, 25), (22, 28), (22, 29), (22, 31), (22, 32), (24, 25),
                  (24, 26), (24, 39), (24, 40), (24, 41), (25, 26), (25, 27), (25, 28),
                  (26, 27), (26, 30), (26, 39), (27, 28), (27, 30), (28, 29), (28, 30),
                  (29, 30), (29, 31), (30, 31), (30, 33), (30, 38), (30, 39), (31, 32),
                  (31, 33), (32, 33), (33, 34), (33, 36), (33, 38), (34, 36), (35, 36),
                  (35, 45), (36, 37), (36, 38), (36, 45), (37, 38), (38, 39), (39, 41),
                  (40, 41), (41, 42), (41, 43), (41, 44), (42, 43), (42, 44), (42, 45),
                  (43, 44), (44, 45)],
        "coordinates": [(18, 147), (48, 83), (64, 90), (47, 28), (63, 34),
                        (78, 55), (82, 74), (84, 80), (82, 69), (100, 78),
                        (94, 97), (110, 162), (116, 144), (143, 149), (140, 111),
                        (137, 102), (136, 95), (122, 78), (110, 67), (112, 60),
                        (98, 59), (93, 55), (102, 35), (108, 14), (130, 22),
                        (125, 32), (128, 37), (127, 40), (122, 42), (118, 47),
                        (127, 48), (116, 53), (111, 54), (122, 57), (124, 65),
                        (146, 87), (158, 65), (148, 57), (138, 54), (137, 41),
                        (160, 13), (168, 29), (189, 39), (194, 32), (202, 33),
                        (191, 118)]}

COLOR = 1


def test_coloring(planar_map, coloring: List[Tuple[str, str]]):
    """ Function provided by assignment to test the coloring we found.

    Args:
        planar_map: the current state of our world
        coloring: a list of the coloring of the world that we are testing
    """
    edges = planar_map["edges"]
    nodes = planar_map["nodes"]

    for start, end in edges:
        try:
            assert coloring[start][COLOR] != coloring[end][COLOR]
        except AssertionError:
            print("%s and %s are adjacent but have the same color." % (nodes[start], nodes[end]))


def simplify_map(coloring: Dict[Tuple[int, str], str]) -> List[Tuple[str, str]]:
    """ Converts color assignment dict into a list of tuples.

    This helper function just removes superfluous "index" data from the coloring map
    that is not requested from the test_coloring or draw_map functions since
    they just rely on the order of the list provided.

    Data structure before-
    {(0, 'Fairfield'): 'red', (1, 'Litchfield'): 'blue'}
    Data structure after-
    [('Fairfield', 'red'), ('Litchfield', 'blue')]

    Args:
        coloring: Dict containing node coloring information

    Returns:
        List containing node coloring information, without node index.

    """

    # coloring.keys() must be sorted first so that the nodes maintain the order that draw_map expects
    return [(key[1], coloring[key]) for key in sorted(coloring.keys())]


def assign_and_test_coloring(name: str,
                             planar_map: PlanarMap,
                             colors: List[str],
                             trace: bool = False):
    """ Function to assign test coloring, provided by assignment.

    Args:
        name: Name of the map we are assigning
        planar_map: The current state of our world
        colors: the list of colors we are using to color or map
        trace: bool flag to enable debug printing

    """
    print(f"Trying to assign {len(colors)} colors to {name}")

    # initialize some helper dictionaries that will be used
    planar_map["assignment"] = {}
    planar_map["edges_by_node"] = get_node_to_edges_dict(planar_map)
    planar_map["colors_by_node"] = get_node_to_colors_dict(planar_map, colors)

    coloring = color_map(planar_map, colors, trace=trace)

    if coloring:
        print(f"{len(colors)} colors assigned to {name}.")
        test_coloring(planar_map, coloring)
        draw_map(name, planar_map, (12, 8), coloring)
    else:
        print(f"{name} cannot be colored with {len(colors)} colors.")


def draw_map(name: str, planar_map, size: Tuple[int, int], color_assignments=None):
    """ Function to draw map provided by homework assignment.

    Returns:
        Rendering of the coloring of the map that was created by the program, in PNG format.
    """
    graph = nx.Graph()
    labels: Dict[int, str] = dict(enumerate(planar_map["nodes"]))
    pos: Dict[int, Tuple[int, int]] = dict(enumerate(planar_map["coordinates"]))
    # create a List of Nodes as indices to match the "edges" entry.
    nodes = [n for n in range(0, len(planar_map["nodes"]))]
    if color_assignments:
        colors = [c for n, c in color_assignments]

    else:
        colors = ['red' for c in range(0, len(planar_map["nodes"]))]
    graph.add_nodes_from(nodes)
    graph.add_edges_from(planar_map["edges"])
    plt.figure(figsize=size, dpi=600)
    nx.draw(graph, node_color=colors, with_labels=True, labels=labels, pos=pos)
    plt.savefig(name + ".png")


def get_tuple_from_node(search_node: int, edges_by_node: Dict[Tuple[int, str], List[Tuple[int, int]]]) \
        -> Tuple[int, str]:
    """ Generates a tuple containing a node name and its index, from just the index

    Args:
        search_node: node we are looking for
        edges_by_node: dict containing nodes mapped to their edges

    Returns:
        Tuple containing search_node's name and its index

    """

    return [node for node in edges_by_node.keys() if node[0] == search_node][0]


def add_names_to_node_to_edges(node_to_edges: Dict[Tuple[int, str], List[Tuple[int, int]]]) \
        -> Dict[Tuple[int, str], List[Tuple[Tuple[int, str], Tuple[int, str]]]]:
    """ Helper function to transform the values in the dict to include each node's name as well as index.

    Args:
        node_to_edges: Dict containing a node mapped to a list of its edges

    Returns:
        Dict containing nodes mapped to their edges where all nodes are represented as tuples containing
        the node's index and name.

    """
    for tuples in node_to_edges.values():
        index = 0
        for one_tuple in tuples:
            # Convert the edge Tuple into a Tuple of Tuples
            # Before: (0,1)
            # After: ((0, 'Fairfield'), (1, 'Litchfield')
            first: Tuple[int, str] = (get_tuple_from_node(one_tuple[0], node_to_edges))
            second: Tuple[int, str] = (get_tuple_from_node(one_tuple[1], node_to_edges))
            tuples[index] = (first, second)
            index += 1
    return node_to_edges


def get_node_to_edges_dict(planar_map) -> Dict[Tuple[int, str], List[Tuple[Tuple[int, str], Tuple[int, str]]]]:
    """ Helper function to create a dict that map nodes to their edges.

    This function is used to create the following entry in planar_map:
        edges_by_node: Dict[Tuple[int, str], List[Tuple[Tuple[int, str], Tuple[int, str]]]]

    Each node is the key, representing by its index and name.
    Each key is a list of edges, where each edge is a tuple of two node tuples.

    Args:
        planar_map: The world we are searching

    Returns:
        A Dict containing nodes mapped to their edges

    """
    # creates a dict of node indexes mapped to names
    node_dict: Dict[int, str] = dict(enumerate(planar_map["nodes"]))

    # Creates a dict with nodes mapped to a list of edges for that node.
    # All nodes in the dict are represented as tuple containing an index and a name, for clarity.
    return add_names_to_node_to_edges(
        {(node, node_dict[node]): list(filter(lambda x: node in x, planar_map["edges"])) for node in node_dict})


def get_node_to_colors_dict(planar_map: PlanarMap, colors: List[str]):
    """ Helper function to create a dict that maps nodes to their potential colors (domain).

    Args:
        planar_map:
        colors: the domain of colors

    Returns:
        A dict that contains a node mapped to an initial list of colors, which is its domain.

    """
    return {key: list(colors) for key in dict.fromkeys(planar_map["edges_by_node"].keys())}


if __name__ == "__main__":
    debug = len(sys.argv) > 1 and sys.argv[1].lower() == 'debug'

    # edit these to indicate what you implemented.
    print("Backtracking...", "Yes!")
    print("Forward Checking...", "Yes!")
    print("Minimum Remaining Values...", "Yes!")
    print("Degree Heuristic...", "No.")
    print("Least Constraining Values...", "Yes!")
    print("")

    three_colors: List[str] = ["red", "blue", "green"]
    four_colors: List[str] = ["red", "blue", "green", "yellow"]

    # start our timer
    start_time = time.time()

    # Easy Map
    assign_and_test_coloring("Connecticut", connecticut, four_colors, trace=debug)
    assign_and_test_coloring("Connecticut", connecticut, three_colors, trace=debug)
    # Difficult Map
    assign_and_test_coloring("Europe", europe, four_colors, trace=debug)
    assign_and_test_coloring("Europe", europe, three_colors, trace=debug)

    if debug:
        print(f'\n\nProgram Execution Duration')
        print(f' --- {round(time.time() - start_time, 2)} seconds ---')
