#!/usr/bin/python3
from collections import defaultdict
from operator import itemgetter

from geolite2 import geolite2

reader = geolite2.reader()
country = defaultdict(lambda: 0)
with open('aci-proxy-list-ips.txt', 'r') as file:
    for line in file:
        line = line.strip()
        response = reader.get(line)
        try:
            name = response['country']['names']['en']
            # print(name)
            if name is not None:
                country[response['country']['names']['en']] += 1
        except:
            pass

sorted_counts = sorted(country.items(), key=itemgetter(1), reverse=True)
highest = next(iter(sorted_counts))
print(f'country: {highest[0]} count: {highest[1]}')
