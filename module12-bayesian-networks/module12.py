from __future__ import division

import csv
import random
from copy import deepcopy

import numpy as np


def read_csv_file(filename):
    """ Read in the data from the provided file and return as two shuffled sets of data.

    The single character domain specifier is replaced by the full name of the domain in this function.

    Args:
        filename: Provided file name to read from.

    Returns:
        Two shuffled lists of input data from the provided filename.
    """
    rows = []
    field_domains = [
            {'p': 'poisonous', 'e': 'edible'},
            {'b': 'bell', 'c': 'conical', 'x': 'convex', 'f': 'flat', 'k': 'knobbed', 's': 'sunken'},
            {'f': 'fibrous', 'g': 'grooves', 'y': 'scaly', 's': 'smooth'},
            {'n': 'brown', 'b': 'buff', 'c': 'cinnamon', 'g': 'gray', 'r': 'green', 'p': 'pink', 'u': 'purple',
             'e': 'red',
             'w': 'white', 'y': 'yellow'},
            {'t': 'bruises', 'f': 'no'},
            {'a': 'almond', 'l': 'anise', 'c': 'creosote', 'y': 'fishy', 'f': 'foul', 'm': 'musty', 'n': 'none',
             'p': 'pungent', 's': 'spicy'},
            {'a': 'attached', 'd': 'descending', 'f': 'free', 'n': 'notched'},
            {'c': 'close', 'w': 'crowded', 'd': 'distant'},
            {'b': 'broad', 'n': 'narrow'},
            {'k': 'black', 'n': 'brown', 'b': 'buff', 'h': 'chocolate', 'g': 'gray', 'r': 'green', 'o': 'orange',
             'p': 'pink', 'u': 'purple', 'e': 'red', 'w': 'white', 'y': 'yellow'},
            {'e': 'enlarging', 't': 'tapering'},
            {'b': 'bulbous', 'c': 'club', 'u': 'cup', 'e': 'equal', 'z': 'rhizomorphs', 'r': 'rooted', '?': 'missing'},
            {'f': 'fibrous', 'y': 'scaly', 'k': 'silky', 's': 'smooth'},
            {'f': 'fibrous', 'y': 'scaly', 'k': 'silky', 's': 'smooth'},
            {'n': 'brown', 'b': 'buff', 'c': 'cinnamon', 'g': 'gray', 'o': 'orange', 'p': 'pink', 'e': 'red',
             'w': 'white',
             'y': 'yellow'},
            {'n': 'brown', 'b': 'buff', 'c': 'cinnamon', 'g': 'gray', 'o': 'orange', 'p': 'pink', 'e': 'red',
             'w': 'white',
             'y': 'yellow'},
            {'p': 'partial', 'u': 'universal'},
            {'n': 'brown', 'o': 'orange', 'w': 'white', 'y': 'yellow'},
            {'n': 'none', 'o': 'one', 't': 'two'},
            {'c': 'cobwebby', 'e': 'evanescent', 'f': 'flaring', 'l': 'large', 'n': 'none', 'p': 'pendant',
             's': 'sheathing', 'z': 'zone'},
            {'k': 'black', 'n': 'brown', 'b': 'buff', 'h': 'chocolate', 'r': 'green', 'o': 'orange', 'u': 'purple',
             'w': 'white', 'y': 'yellow'},
            {'a': 'abundant', 'c': 'clustered', 'n': 'numerous', 's': 'scattered', 'v': 'several', 'y': 'solitary'},
            {'g': 'grasses', 'l': 'leaves', 'm': 'meadows', 'p': 'paths', 'u': 'urban', 'w': 'waste', 'd': 'woods'},
    ]

    # read in the data, replacing the single character domain with the full word
    with open(filename, 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
            new_row = []
            for i in range(0, len(row)):
                new_row.append(field_domains[i].get(row[i]))
            rows.append(new_row)

    # randomize the rows of input
    random.shuffle(rows)

    # return two sets of data
    set1, set2 = np.array_split(rows, 2)
    return set1.tolist(), set2.tolist()


def read_self_check():
    self_check = [['no', 'round', 'large', 'blue'],
                  ['yes', 'square', 'large', 'green'],
                  ['no', 'square', 'small', 'red'],
                  ['yes', 'round', 'large', 'red'],
                  ['no', 'square', 'small', 'blue'],
                  ['no', 'round', 'small', 'blue'],
                  ['yes', 'round', 'small', 'red'],
                  ['no', 'square', 'small', 'green'],
                  ['yes', 'round', 'large', 'green'],
                  ['yes', 'square', 'large', 'green'],
                  ['no', 'square', 'large', 'red'],
                  ['yes', 'square', 'large', 'green'],
                  ['yes', 'round', 'large', 'red'],
                  ['no', 'square', 'small', 'red'],
                  ['no', 'round', 'small', 'green']]
    return self_check


def domain_of(attribute, data, attributes):
    """ Returns the domain of an given attribute

    Args:
        attribute: The attribute who's domain we are searching for
        data: The set of data we're searching
        attributes: The set of attributes in our data

    Returns:
        List containing the domain of a given attribute.

    """
    domain = set()
    index = attributes.index(attribute)
    for entry in data:
        domain.add(entry[index])
    return list(domain)


def get_subset_of_data(value, attribute, data, attributes):
    """ Function to return a subset of data that contains a specific value for an attribute.

    Args:
        value: The specified value our subset should have.
        attribute: The specified attribute for the value.
        data: The data set from which we will create a subset.
        attributes: The set of all attributes.

    Returns:
        A subset of only the data which contains a specified value for a specified attribute.

    """
    index = attributes.index(attribute)
    subset = []
    for entry in data:
        if entry[index] == value:
            subset.append(entry)
    subset = deepcopy(subset)
    return subset


def get_count_of(class_label, attribute_subset) -> int:
    """ Helper function to retrieve the count of a particular classifier from a subset of data.

    Args:
        class_label: The class label we're counting
        attribute_subset: The subset of data we're searching through

    Returns:
        A count of the class labels that occur in the subset.
    """
    count = 0
    for entry in attribute_subset:
        if entry[0] == class_label:
            count += 1
    return count


def get_labels():
    """ Helper function to return the class labels which is useful in the event that there is only one test instance.

    Returns:
        A list containing the class labels for the data set.

    """
    return ['poisonous', 'edible']


def naive_bayes(data, field_names):
    """ Calculates the probabilities for each attribute as a proportion of the entire dataset, as in the self-check.

    Args:
        data: The provided data.
        field_names: A list of the field names in our dataset.

    Returns:
        Dict containing probabilities of classifications for each attribute.
    """
    prob = {}
    no = get_labels()[0]
    yes = get_labels()[1]
    total_no_count = get_count_of(no, get_subset_of_data(no, field_names[0], data, field_names))
    total_yes_count = get_count_of(yes, get_subset_of_data(yes, field_names[0], data, field_names))
    for attribute in field_names:
        attr_prob = {}
        for attribute_value in domain_of(attribute, data, field_names):
            attribute_subset = get_subset_of_data(attribute_value, attribute, data, field_names)
            value_prob = {}
            if attribute == field_names[0]:
                # use total for class labels
                sum_yes = total_no_count + total_yes_count
                sum_no = sum_yes
                value_prob[yes] = get_count_of(yes, attribute_subset) / sum_yes
                value_prob[no] = get_count_of(no, attribute_subset) / sum_no
            else:
                value_prob[yes] = (get_count_of(yes, attribute_subset) + 1) / (total_yes_count + 1)
                value_prob[no] = (get_count_of(no, attribute_subset) + 1) / (total_no_count + 1)

            attr_prob[attribute_value] = value_prob
        prob[attribute] = attr_prob
    return prob


def probability_of(instance, label, probs):
    """ Function that implements the sigma notation product portion of the pseudocode.

    This function loops through all of the attributes of a given instance and creates a
    "running product" of all of the probabilities.

    Args:
        instance: The instance for which we are calculating probabilities.
        label: The class label for the instance.
        probs: The probabilities generated by our naive Bayes algorithm.

    Returns:

    """
    # only loop through the attributes, and not the class labels

    probability_product = 1.0

    # class probability
    probability_product *= probs[field_names[0]][label][label]

    # attribute probabilities
    for attribute in range(1, len(instance)):
        try:
            probability_product *= probs[field_names[attribute]][instance[attribute]][label]
        except KeyError:
            # Occasionally, one test set would contain a permutation not found in the other, resulting in a KeyError.
            # When this happens, we ignore it and continue our product calculation.
            pass
    return probability_product


def normalize(results):
    """ Normalize the test data by transforming it to a percentage.

    Args:
        results: Results generated by our naive Bayes algorithm.

    Returns:
        List of normalized tuples.
    """
    normalized = []
    for label, result in results:
        normalized.append((label, result / (results[0][1] + results[1][1])))
    return normalized


# *****************************************************************************************
# required functions below
# *****************************************************************************************

field_names = ['edible', 'cap-shape', 'cap-surface', 'cap-color', 'bruises', 'odor', 'gill-attachment',
               'gill-spacing',
               'gill-size', 'gill-color', 'stalk-shape', 'stalk-root', 'stalk-surface-above-ring',
               'stalk-surface-below-ring', 'stalk-color-above-ring', 'stalk-color-below-ring', 'veil-type',
               'veil-color', 'ring-number', 'ring-type', 'spore-print-color', 'population', 'habitat']
self_check_field_names = ['safe', 'shape', 'size', 'color']


def learn(training_data):
    """ Calculates all of the necessary probabilities of the attributes using +1 smoothing.

    Args:
        training_data: The provided training data. The "class" is expected at the first index of each row.

    Returns:
        Dict containing probabilities of classifications for each attribute.
    """

    return naive_bayes(training_data, field_names)


def classify(probabilities, instances):
    """ Returns a list of classifications using the data from our naive bayes classifer.

    Args:
        probabilities: The probabilities calculated through "learning" from the training data.
        instances: The test instances we will be assigning probabilities.

    Returns:
        A list tuples containing the normalized probabilities for each input test instance.
    """
    results = []
    for instance in instances:
        instance_results = []
        for label in get_labels():
            instance_results.append((label, probability_of(instance, label, probabilities)))
        instance_results = normalize(instance_results)

        # sort the results, so the best is first
        instance_results.sort(reverse=True, key=lambda x: x[1])
        results.append(instance_results)

    return results


def evaluate(test_data, bayes_results):
    """ This function compares the test_data classifications to the created classifications and returns the error-rate.

    Args:
        test_data: Test data from agaricus-lepiota.data
        bayes_results: Generated probabilities from naive Bayes

    Returns:

    """
    test_classes = [row[0] for row in test_data]
    errors = 0
    for item1, item2 in zip(test_classes, bayes_results):
        if item1 != item2[0][0]:
            errors += 1
    return errors / len(test_data)


verbose = False

# --------------------------------------------------------------
# code to run the self-check data
# makes sure to change the get_labels() function above...
# --------------------------------------------------------------
# set1_self = read_self_check()
# set2_self = read_self_check()
# bayes = learn(set1_self)
# self_check_test_set = [['no', 'square', 'large', 'red']]
# self_check_test_set2 = [['yes', 'round', 'small', 'red']]
# results = classify(bayes, self_check_test_set)
# print(results)
# error_rate = evaluate(self_check_test_set, results)
# print(f'Error rate: {error_rate}')
# print()
# results = classify(bayes, set2_self)
# print(results)
# error_rate = evaluate(set2_self, results)
# print(f'Error rate: {error_rate}')
# print()

set1, set2 = read_csv_file('agaricus-lepiota.data')

bayes_1 = learn(set1)
results_1 = classify(bayes_1, set2)
error_rate_1 = evaluate(set2, results_1)
print(f'Error rate for set 1: {error_rate_1}')

bayes_2 = learn(set2)
results_2 = classify(bayes_2, set1)
error_rate_2 = evaluate(set1, results_2)
print(f'Error rate for set 2: {error_rate_2}')
print(f'Average Error Rate: {(error_rate_1 + error_rate_2) / 2}')
