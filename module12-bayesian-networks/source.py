#!/usr/bin/python3
import os
import binascii
import random

words = [
    "ALPHA",
    "BRAVO",
    "CHARLIE",
    "DELTA",
    "ECHO",
    "FOXTROT",
    "GOLF",
    "HOTEL",
    "INDIA",
    "SIERRA",
    "TANGO",
    "ZETA",
]

INTRO = "The following encoded individuals are to be given a $27.3k bonus:".ljust(63) + "\n"
OUTRO = "Furthermore, the FLAG is:".ljust(63) + "\n"

def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]

def encrypt_otp(data):
    d = data.encode('utf-8')
    # otp = bytes(1)
    # otp = bytes(os.urandom(64))
    out = []
    # print(data)
    # print(d)
    # print(f'len(d): {len(d)}')

    for i in range(0, len(d)):
        val1 = d[i]
        val2 = otp[i % len(otp)]
        result = val1 ^ val2
        out.append(val1 ^ val2)
    print("post enc")
    # print(out)

    return bytes(out)
def decrypt_opt(bytes2, otp):
    # out = []
    bytes = list(bytes2)
    out = []
    for i in range(0,len(bytes)):
        # print(bytes[i])
        # print(int(bytes[i]))
        # val1 = d[i]
        # val2 = otp[i % len(otp)]
        # result = val1 ^ val2
        out.append(int(bytes[i] ^ int(otp[i % len(otp)])))
    out2 = bytearray(out)
    out3 = ''
    try:
        out3 = out2.decode('utf-8')
    except:
        out3 = 'fails'
    # out = [ chr(x) for x in out]

    return out3
def generate_line(length):
    out = ''
    while len(out) < length:
        out += random.choice(words) + " "

    out = out[0:length-1]
    out = ' '.join(out.split(" ")[0:-1])
    return out.ljust(length-1) + '\n'


def generate_doc(flag):
    out = INTRO

    for i in range(0,10):
        out += generate_line(64)

    out += " " * 63 + "\n"
    out += OUTRO + "\n"
    out += flag.ljust(63) + "\n"
    return out

otp = bytes(os.urandom(64))
print(otp)
flag = "redacted"
doc = generate_doc(flag)
enc = encrypt_otp(doc)
print(enc)
# print(enc)
# print(list(enc))
chunks = list(divide_chunks(enc, 64))
# print(enc)
dec = decrypt_opt(enc, otp)
#
# out = b''
# for line in enc:
#     out += binascii.hexlify(line) + b"\n"
f2 = open("./origfile")
lines = f2.readlines()
newinput=''
for line in lines:
    newinput+=line.strip()
print(newinput)
decrypted = ''

while not 'ACI' in decrypted:
    otp = bytes(os.urandom(64))
    otp = """ALPHABRAVOCHARLIEDELTAECHOFOXTROTGOLFHOTELINDIASIERRATANGOZERTA\n"""
    print(otp)
    bytes_unhex = binascii.unhexlify(newinput)
    bytes_list = list(bytes_unhex)
    bytes2 = [ int(thing) for thing in bytes_unhex ]
    # decrypted = decrypt_opt(newinput, otp)
    decrypted = decrypt_opt(bytes_unhex, otp)
    # print(decrypted)
    # print()
print()
# f = open("./document.encrypted", "wb")
# f.write(out)
# f.close()


