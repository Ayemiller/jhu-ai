#!/usr/bin/python3
import argparse
import binascii
import socket
# 'argparse' is a very useful library for building python tools that are easy
# to use from the command line.  It greatly simplifies the input validation
# and "usage" prompts which really help when trying to debug your own code.
from base64 import b64decode, b64encode

from bitstring import BitArray


def text_to_bits(text, encoding='utf-8', errors='surrogatepass'):
    bits = bin(int(binascii.hexlify(text.encode(encoding, errors)), 16))[2:]
    return bits.zfill(8 * ((len(bits) + 7) // 8))


def text_from_bits(bits, encoding='utf-8', errors='surrogatepass'):
    n = int(bits, 2)
    return int2bytes(n).decode(encoding, errors)


def int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))


def oct2bytes(i):
    hex_string = '%o' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))


# def int2bytes(i):
#     hex_value = '{0:x}'.format(i)
#     # make length of hex_value a multiple of two
#     hex_value = '0' * (len(hex_value) % 2) + hex_value
#     return codecs.decode(hex_value, 'hex_codec')

parser = argparse.ArgumentParser(description="Solver for 'All Your Base' challenge")
# parser.add_argument("ip", help="IP (or hostname) of remote instance")
# parser.add_argument("port", type=int, help="port for remote instance")
args = parser.parse_args();
args.ip = "challenge.acictf.com"
args.port = 52062
args.port = 45713
args.port = 21191

# This tells the computer that we want a new TCP "socket"
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# This says we want to connect to the given IP and port
sock.connect((args.ip, args.port))

# This gives us a file-like view for receiving data from the connection which
# makes handling messages from the server easier since it handles the
# buffering of lines for you.  Note that this only helps us on receiving data
# from the server and we still need to send data over the underlying socket
# (i.e. `sock.send(...)` at the end of the loop below).
f = sock.makefile()

# bin to oct works
# raw to hex works
# dec to raw
# question = '4108349287800430591794621687850962391011962334270929526271421373715390189405360837390844975130126997381783365541405017759993177985859340781733640831925299'
# Nq1iPe.WWB!ngUT&+8DqgQNYa=Q;Ljy5XylqwhVbWkwUOh_V<#>UNy`-l8Ml7r|3

# hex to bin
# question = '2b2622307d7a5b2a214b2e774c4f7a3e426678214a40544b7271684b632353263474617a3d3d5f2e5152522c30666d7b7454786074745b333e23512d53697c32'
# 101011001001100010001000110000011111010111101001011011001010100010000101001011001011100111011101001100010011110111101000111110010000100110011001111000001000010100101001000000010101000100101101110010011100010110100001001011011000110010001101010011001001100011010001110100011000010111101000111101001111010101111100101110010100010101001001010010001011000011000001100110011011010111101101110100010101000111100001100000011101000111010001011011001100110011111000100011010100010010110101010011011010010111110000110010

# hex to oct
# question = '264a5d21257155784f5b213e764e5b254e6d6d266f3437775f7a36733f242530475c506a273e434846687a4334343b77754f732c56336164322f41443255606e'
# 46224564411127052536047533102371662345544523466555114674641567353736433163176220451404353424065047174415102146417220632064166735652367145425431541310310572024206225260156

# hex to raw
# question = '44393b4d4b395e6d2557436e5d2f53292d67453e714e33277c5c4f45365626696f5351643759386b414e55235a29522f3d50493f6d7d3b59374c39763a52437d'
# D9;MK9^m%WCn]/S)-gE>qN3'|\OE6V&ioSQd7Y8kANU#Z)R/=PI?m};Y7L9v:RC}

# dec to b64
question = '6294997433490595845709745329378974540229947923583898236333995622540251450659255829007466716674827117321605002752105562870694947024616662313863879054868530'


# eDFOd1R7LiR9Xmsqa0l2dyk0bSJVTmpdcnsjMXEvI1gveHZZQXAwXisrNnZHYHVjSGpQVT88KjNfL0p2WHxAMg==

# new_question = bytes(str(question), encoding='ascii')  # , encoding='utf-8')
# new_question = bytearray(question.encode())
# new_question = struct.pack(">I", question)
# new_question = bytes(str(question), encoding='utf-8')
# new_question = BitArray('0d' + question)
# new_question = bytes(str(int(question)), encoding='utf-8')
# new_question = int(question).to_bytes(99, 'big')

# new_question = int2bytes(int(question))
# new_question = BitArray(new_question)
# answer = b64encode(new_question.bytes)
# answer = str(answer)
# answer = answer[2:]
# answer = str(answer) + str('\n')
# answer = answer.encode()
# print(answer)
# print()


# question = '100101001100100101001000111000011100110010101101101101010100100011111101001001001111100100001000110110001100000011101001000000001001000111011001001010010000000111000100100010010111110101011001110011010010100101110001000001011001010110110001101001011001100100001001001011011000010101001101100000011110010010100100100010010001100110011000110101010111100110010100100100011111010111010001101010001101000110010000110001010010010110111101100111001000110111010001100111001101010111011101000101001110110110000001101001'
# # from bin to dec
# # incorreect -1403809688993554204008613768829759692780396185356844195695868965136468586216132559165385202146942141596128407948595636439088835631644897511518074854219671
# # answer: That is incorrect.  I was expecting:
# answer = '1948142293492095070884892480721701839089445269791254148735021395793972421302254185035083372394783715326379556598025876274349635071341744975090337397301353'
# new_question = BitArray('0b' + question)
#
# answer = new_question.uint
# answer = str(answer) + str('\n')
# answer = answer.encode()
# print(answer)
# print()

# question = '111001011001110101111101011101010001010100000100100100010000110100111100110111001100010100001000110001011010100010110001010100010011110100011101110011010100010100011101010010010000110101000101110000001001110110101001011110011010010011100001011110011011010101111000110110001010100101111001011100001001100101100000101100001101100100111001001101010110110011101101111010011011000110001100111110011000100010010001110000001000100100100101101101011100110101010001101000001101100111101101011011001010010101110001011001'
# # from bin to raw
# answer = BitArray('0b' + question)
# answer = bytes(str(answer) + str('\n'), encoding='ascii')
# print(answer)
# print()


def remove_leading_zeros(answer):
    while answer[0] == '0' and len(answer) > 1:
        answer = answer[1:]
    return answer


# question = '1000000011111000111010000110111001100100100011000111101010010010100000000100010011010010011110100111000010101110100101000111011001110100110001001100011010011010110010001001011001110110010110001010110010110010011111001001110001001010100110001011000010011100100000101011001010101110100011000110111011110100101100001001100011000010110101101001011011010110110010000111000011010000110101100100100010100100100110101100000010010010100001100101010011100100110000001100110011110000011111100100110011101110010011001001100'
# # from bin to raw
# # INCORRECT b'0x80f8e86e648c7a928044d27a70ae947674c4c69ac8967658acb27c9c4a98b09c82b2ae8c6ef4b098c2d696d6c870d0d648a49ac0928654e4c0ccf07e4cee4c9, 0b100\n'
# # answer: That is incorrect.  I was expecting:
# # CORRECT: @|t72F=I@"i=8WJ;:bcMdK;,VY>N%LXNAYWF7zXLakKkd8hk$RM`IC*r`fx?&w&L
# answer = BitArray('0b' + question)
# bitmod = len(answer) % 8
# diff = BitArray(8 - bitmod, fill=0)
# answer = diff + answer
# answer = str(binascii.b2a_qp(answer.bytes))
# answer = answer[2:]
# answer = answer[:-1]
# answer = answer + '\n'
# print(answer.encode())
# print()

question = '504f7d7a50457d775c2e58772e746d7268776e54546a617c712655437125676e502353313252767c5133724875626b2a693d5b42215b4d406b5c705f43394e4c'
# from hex to raw

# INCORRECT =00PO}zPE}w\\.Xw.tmrhwnTTja|q&UCq%gnP#S12Rv|Q3rHubk*i=3D[B![M@k\\p_C9NL
# CORRECT PO}zPE}w\.Xw.tmrhwnTTja|q&UCq%gnP#S12Rv|Q3rHubk*i=[B![M@k\p_C9NL
# answer = BitArray('0x' + question)
# bitmod = len(answer) % 8
# diff = BitArray(8 - bitmod, fill=0)
# answer = diff + answer
# answer = str(binascii.b2a_qp(answer.bytes))
# answer = answer[3:]
# answer = answer[:-1]
# answer = answer + '\n'
# answer = remove_leading_zeros(answer)
# print(answer.encode())
# print()


question = '2d2264617960776c7c785a59513c41296b4726632666256c216b2f3a6c79452d27736e5b3c386942593b4a532e2b667372477d415e5b4d4869527d3f67446347'
# from hex to b64
# MINE AC0iZGF5YHdsfHhaWVE8QSlrRyZjJmYlbCFrLzpseUUtJ3NuWzw4aUJZO0pTLitmc3JHfUFeW01IaVJ9P2dEY0c=

# CORRECT LSJkYXlgd2x8eFpZUTxBKWtHJmMmZiVsIWsvOmx5RS0nc25bPDhpQlk7SlMuK2Zzckd9QV5bTUhpUn0/Z0RjRw==
# AC0iZGF5YHdsfHhaWVE8QSlrRyZjJmYlbCFrLzpseUUtJ3NuWzw4aUJZO0pTLitmc3JHfUFeW01IaVJ9P2dEY0c =
# question = '110111010111100110111101111011001110110100110001100001001001110010111001100101011100110110010001000101001100000111010101101011011100000110010101111100001010100010110101111101001111100100010001111100001000010011111100101111011101100111000100101101011001010101101101100100011010010100010001101100010111100101000101010100011000010110001100111101010011000101110101110110011101100100010100110010010111110110010001100000011101100101000000111100010101110100011100101100001011100011110001000000010101100110011000101001'
# # from bin to b64
# answer = BitArray('0x' + question)
# bitmod = len(answer) % 8
# if bitmod != 0:
#     diff = BitArray(8 - bitmod, fill=0)
#     answer = diff + answer
# answer = b64encode(answer.bytes)
# answer = str(answer)
# answer = answer[2:]
# answer = answer[:-1]  # get rid of the bit shit
# answer = str(answer) + str('\n')
# print(answer.encode())
# print()

question = 'TTUsfDY3e09MX2I5OU1sQXg6UHkkJFlWaHQ8c3VAaWE0PmZebGpKcUpuNHI+XmUuQyp9bmdhWjR4Y0RFMGp8OA=='
# from b64 to raw
# INCORRECT 5,|67{OL_b99MlAx:Py$$YVht<su@ia4>f^ljJqJn4r>^e.C*}ngaZ4xcDE0j|8
# CORRECT M5,|67{OL_b99MlAx:Py$$YVht<su@ia4>f^ljJqJn4r>^e.C*}ngaZ4xcDE0j|8
# answer = b64decode(question)
# answer = BitArray(answer)
# bitmod = len(answer) % 8
# if bitmod != 0:
#     diff = BitArray(8 - bitmod, fill=0)
#     answer = diff + answer
# answer = str(binascii.b2a_qp(answer.bytes))
# answer = answer[2:]
# answer = answer[:-1]
# answer = answer + '\n'
# answer = remove_leading_zeros(answer)
# print(answer.encode())


# question = '7d474a4d26364b72765326592f2e566a34543c242e24753d566f60677b4e68703f3d6f2a665c487c2342757d7a675634525726357d5b6e4826742b5740294855'
# # from hex to raw
# # wrong      }GJM&6KrvS&Y/.Vj4T<$.$u=3DVo`g{Nhp?=3Do*f\\H|#Bu}zgV4RW&5}[nH&t+W@)HU
#
# # correct    }GJM&6KrvS&Y/.Vj4T<$.$u=Vo`g{Nhp?=o*f\H|#Bu}zgV4RW&5}[nH&t+W@)HU
# #            }GJM&6KrvS&Y/.Vj4T<$.$u=3DVo`g{Nhp?=3Do*f\\\\H|#Bu}zgV4RW&5}[nH&t+W@)HU
# # }GJM&6KrvS&Y/.Vj4T<$.$u=3DVo`g{Nhp?=3Do*f\\\\H|#Bu}zgV4RW&5}[nH&t+W@)HU'\n"
# answer = BitArray('0x' + question)
# bitmod = len(answer.bytes) % 8
# if bitmod != 0:
#     diff = BitArray(8 - bitmod, fill=0)
#     answer = diff + answer
# answer = str(binascii.b2a_qp(answer.tobytes()))
# answer = answer[2:]
# if not 'hex' != 'b64':
#     answer = answer[1:]
# answer = answer[:-1]
# answer = answer + '\n'
# answer = remove_leading_zeros(answer)
# print(answer.encode())
# print()


# question = '153266354652243012234075175276651013422706214631545260571542606444333050503234535743702345333263107340735421142716032022501256701401765711220047467304514743526413134237163'
# # from oct to b64
# # WRONG  AGtbOzVKMFJwen1fakFxLjIzM2VYXmxYaSNsUUNOV3x8JyttZkdwd2ImLnBoJUFXcGA/XkpATzdiUzx1aFlxPnM=
# # RIGHT  a1s7NUowUnB6fV9qQXEuMjMzZVhebFhpI2xRQ05XfHwnK21mR3B3YiYucGglQVdwYD9eSkBPN2JTPHVoWXE+cw==
# # answer: That is incorrect.  I was expecting:
# answer = BitArray('0o' + question)
# bitmod = len(answer) % 8
# if bitmod != 0:
#     diff = BitArray(8 - bitmod, fill=0)
#     answer = diff + answer
# answer2 = codecs.encode(answer.bytes, 'base64')
# print(answer2)
# answer44 = b64encode(answer.tobytes())
# answer33 = b85encode(answer.tobytes())
# print(answer33)
# answer = str(answer) + str('\n')
# print(answer)
# print()

# FAILED ON
# from oct to b64
question = '156262511263545215033221506124421643307610335441476130501233206045420654564144460671604310322437051220450612444452612623515352731533322645132656175236645633345215335034532'
new_question = int2bytes(int(question, 8))
new_question = BitArray(new_question)
# new_question = BitArray('0o' + question)

answer = new_question
bitmod = len(answer) % 8
if bitmod != 0:
    diff = BitArray(8 - bitmod, fill=0)
    answer = diff + answer
answer = b64encode(answer.bytes)
# answer = standard_b64encode(bytearray(answer.tobytes()))
# if question_enc == 'oct':
#     pass
# else:
answer = answer[2:]
answer = answer[:-1]  # get rid of the bit shit
answer = str(answer) + str('\n')
print(answer.encode())
# WRONG
# b'AG5ZUlZ2VGhtI0YqRHRsfEN2Qz4sUFNoYSxDWXQyTDc4RkNKPilISjFSSVYrJ011dmttLSlrXH1PaXNuVGt0OVo=\n'

# CORRECT
# bllSVnZUaG0jRipEdGx8Q3ZDPixQU2hhLENZdDJMNzhGQ0o + KUhKMVJJVisnTXV2a20tKWtcfU9pc25Ua3Q5Wg ==

# answer = str('abc' + '\n')
# sock.send(answer.encode())
line = True
last_line = ''
while not (line == '' and last_line == ''):
    last_line = line
    line = f.readline().strip()
    print(line)
    # This iterates over data from the server a line at a time.  This can
    # cause some unexpected behavior like not seeing "prompts" until after
    # you've sent a reply for it (for example, you won't see "answer:" for
    # this problem). However, you can still "sock.send" below to transmit data
    # and the server will handle it correctly.
    if line == '------------------------------------------------------------------------------':
        encoding = f.readline().strip().split()
        question_enc = encoding[0]
        answer_enc = encoding[2]

        question = f.readline().strip()

        print(f'converting {question} from {question_enc} to {answer_enc}')
        if question_enc == 'hex':
            new_question = BitArray('0x' + question)  # int(question,16)
        elif question_enc == 'dec':
            new_question = int2bytes(int(question))
            new_question = BitArray(new_question)
        elif question_enc == 'oct':
            new_question = int2bytes(int(question, 8))
            new_question = BitArray(new_question)
            # print(question)
            # new_question = BitArray('0o' + question)
            # print(new_question.oct)
        elif question_enc == 'bin':
            new_question = BitArray('0b' + question)
        elif question_enc == 'b64':
            base64 = b64decode(question)
            new_question = BitArray(base64)
        else:
            new_question = bytes(str(question), encoding='utf-8')
            new_question = BitArray(new_question)

        if answer_enc == 'hex':
            answer = new_question
            bitmod = len(answer) % 4
            if bitmod != 0:
                diff = BitArray(4 - bitmod, fill=0)
                answer = diff + answer
            answer = str(answer.hex + '\n')
            answer = remove_leading_zeros(answer)
            sock.send(answer.encode())
        elif answer_enc == 'dec':
            answer = new_question.uint
            sock.send((str(answer) + '\n').encode())
        elif answer_enc == 'oct':
            bitmod = len(new_question) % 3
            if bitmod != 0:
                diff = BitArray(3 - bitmod, fill=0)
                new_question = diff + new_question
            answer = str(new_question.oct + '\n')
            answer = remove_leading_zeros(answer)
            sock.send(answer.encode())
        elif answer_enc == 'bin':
            answer = str(new_question.bin + '\n')
            answer = remove_leading_zeros(answer)
            sock.send(answer.encode())
        elif answer_enc == 'b64':
            answer = new_question
            bitmod = len(answer) % 8
            if bitmod != 0:
                diff = BitArray(8 - bitmod, fill=0)
                answer = diff + answer
            answer = b64encode(answer.bytes)
            answer = str(answer)
            # if question_enc == 'oct':
            #     pass
            # else:
            answer = answer[2:]
            answer = answer[:-1]  # get rid of the bit shit
            answer = str(answer) + str('\n')
            sock.send(answer.encode())
        else:
            answer = new_question
            bitmod = len(answer) % 8
            if bitmod != 0:
                diff = BitArray(8 - bitmod, fill=0)
                answer = diff + answer
            # answer = str(binascii.b2a_qp(answer.bytes))
            print('sending...')
            print(answer.bytes + '\n'.encode())
            sock.send(answer.bytes + '\n'.encode())
            # answer = str(quopri.decodestring(answer.bytes))
            # answer = answer[2:]
            # if question_enc != 'b64' and question_enc != 'hex':
            #     answer = answer[1:]
            # answer = answer[:-1]
            # answer = answer + '\n'
            # answer = remove_leading_zeros(answer)
            # sock.send(answer.encode('utf-8'))

        print(str(answer).encode('utf-8'))
        # sock.send(answer.bytes + '\n'.encode())

        f.readline().strip()
        # Handle the information from the server to extact the problem and build
        # the answer string.
        pass  # Fill this in with your logic
        # A good starting point for approaching the problem:
        #   1) Identify and capture the text of each question (the "----" lines
        #          should be useful for this).
        #   2) Extract the three primary parts of each question:
        #      a) The source encoding
        #      b) The destination encoding
        #      c) The source data
        #   3) Convert the source data to some "standard" encoding (like 'raw')
        #   4) Convert the "standardized" data to the destination encoding

        # Send a response back to the server
        answer = "Clearly not the answer..."
        # The "\n" is important for the server's
        # interpretation of your answer, so make
        # sure there is only one sent for each
        # answer.
