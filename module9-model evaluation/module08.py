# %%

import random

import numpy as np

clean_data = {
        "plains": [
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, "plains"]
        ],
        "forest": [
                [0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, "forest"],
                [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, "forest"],
                [1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, "forest"],
                [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, "forest"]
        ],
        "hills": [
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, "hills"],
                [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, "hills"],
                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, "hills"],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, "hills"]
        ],
        "swamp": [
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, "swamp"],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, "swamp"]
        ]
}


def blur(data):
    def apply_noise(value):
        if value < 0.5:
            v = random.gauss(0.10, 0.05)
            if v < 0.0:
                return 0.0
            if v > 0.75:
                return 0.75
            return v
        else:
            v = random.gauss(0.90, 0.10)
            if v < 0.25:
                return 0.25
            if v > 1.00:
                return 1.00
            return v

    noisy_readings = [apply_noise(v) for v in data[0:-1]]
    return noisy_readings + [data[-1]]


def generate_data(clean_data, n, key_label):
    labels = set(clean_data.keys())
    labels.remove(key_label)

    total_per_label = int(n / len(labels))
    data = []
    one_more = True
    for label in labels:

        total_per_label = int(n / len(labels))

        for _ in range(total_per_label):
            if (one_more):
                datum = blur(random.choice(clean_data[label]))
                xs = datum[0:-1]
                data.append((xs, 0))
                one_more = False
            datum = blur(random.choice(clean_data[label]))
            xs = datum[0:-1]
            data.append((xs, 0))

    # create n "label" and code as y=1
    for _ in range(n):
        datum = blur(random.choice(clean_data[key_label]))
        xs = datum[0:-1]
        data.append((xs, 1))
    random.shuffle(data)
    return data


def calc_y_hat(val):
    return 1 / (1 + np.exp(-val))


def calculate_error(y_hat, y):
    return (-y * np.log(y_hat) - (1 - y) * np.log(1 - y_hat)).mean()


def learn_model(data):
    x = np.concatenate((np.ones((len(data[0]), 1)), data[0]), axis=1)
    y = data[1]
    # initialize thetas to 0
    thetas = np.zeros(x.shape[1])

    i = 0
    current_error = 1
    previous_error = 0
    while abs(current_error - previous_error) > 0.0000001:
        y_hat = np.dot(x, thetas)
        y_hat = calc_y_hat(y_hat)
        y_hat = np.dot(x.transpose(), (y_hat - y)) / len(y)
        thetas -= alpha * y_hat

        y_hat = np.dot(x, thetas)
        y_hat = calc_y_hat(y_hat)
        previous_error = current_error
        current_error = calculate_error(y_hat, y)

        if verbose and i % 1000 == 0:
            print(f'Iteration {i} current error={current_error}')
        i += 1
    return thetas


def apply_model(model, test_data, threshold):
    test_data = np.concatenate((np.ones((len(test_data), 1)), test_data), axis=1)
    rounded = calc_y_hat(np.dot(test_data, model))

    # apply the provided thresholds to the conditionals
    greater_than = rounded >= threshold
    less_than = rounded < threshold
    rounded[greater_than] = 1
    rounded[less_than] = 0
    return rounded


def error_rate(actual, predicted):
    """ Function to print the confusion matrix and error rate for the actual vs predicted

    Args:
        actual: The generated test data of hills and not-hills.
        predicted: The predictions of hills based on our model.
    """
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0
    for i in range(len(actual)):
        if actual[i] == 1 and int(predicted[i]) == 1:
            true_positive += 1
        elif actual[i] == 0 and int(predicted[i]) == 1:
            false_positive += 1
        elif actual[i] == 0 and int(predicted[i]) == 0:
            true_negative += 1
        elif actual[i] == 1 and int(predicted[i]) == 0:
            false_negative += 1
    return (false_negative + false_positive) / len(actual)


alpha = 0.1
verbose = False
