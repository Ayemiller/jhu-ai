import matplotlib.pyplot as plt
import numpy as np
from module08 import *


def normal_module8():
    alpha = 0.1
    verbose = False

    data = generate_data(clean_data, 100, "hills")
    model = learn_model(data)
    test_data = generate_data(clean_data, 100, "hills")
    x_test, y_test = zip(*data)

    results = apply_model(model, list(x_test), 0.2)

    # Building the Confusion Matrix
    if verbose:
        print(f'preds: {results}')
        print(f'tests: {y_test}')
        print(f'theta: {model}')
    error_rate(y_test, results)


def split(data):
    x_test, y_test = zip(*data)
    x_folds = np.split(np.array(list(x_test)), 5)
    y_folds = np.split(np.array(list(y_test)), 5)
    return x_folds, y_folds


def answer1_old():
    alpha = 0.1
    verbose = False
    data = generate_data(clean_data, 100, "hills")
    x_folds, y_folds = split(data)
    error_rates = []
    for i in range(1, 5):
        x_test_set = x_folds[i]
        y_test_set = y_folds[i]

        x_training_set = np.delete(x_folds, i, 0).reshape(160, 16)
        y_training_set = np.delete(y_folds, i, 0).reshape(160, )

        model = learn_model((x_training_set, y_training_set))
        results = apply_model(model, x_test_set, 0.5)
        error_rates.append(error_rate(y_test_set, results))
    # error_rate(y_test, results)
    print(error_rates)
    print(f'average: {np.average(error_rates)}')
    print(f'std_dev: {np.std(error_rates)}')


def answer1():
    num_to_generate = 200
    threshold = 0.5
    data = generate_data(clean_data, int(num_to_generate / 2), "hills")
    x_folds, y_folds = split(data)
    error_rates = []
    print('Performing logistic regression, please wait...')
    for i in range(0, 5):
        x_test_set = x_folds[i]
        y_test_set = y_folds[i]

        x_training_set = np.delete(x_folds, i, 0).reshape(int(num_to_generate - num_to_generate / 5), 16)
        y_training_set = np.delete(y_folds, i, 0).reshape(int(num_to_generate - num_to_generate / 5), )

        model = learn_model((x_training_set, y_training_set))
        results = apply_model(model, x_test_set, threshold)
        single_error_rate = error_rate(y_test_set, results)
        error_rates.append(single_error_rate)
        print(f'Error rate testing fold {i + 1}: {single_error_rate}')
    print('Cross Validation Metrics')
    print(f'Average Error Rate: {np.average(error_rates)}')
    print(f'Standard Deviation of Error Rates: {np.std(error_rates)}')


def answer2():
    num_to_generate = 200
    threshold = 0.5
    percent = 20
    folds = 5
    training_size = int(num_to_generate - num_to_generate / folds)
    data = generate_data(clean_data, int(num_to_generate / 2), "hills")
    training_error_rate = {}
    test_error_rate = {}
    x_folds, y_folds = split(data)
    error_rates = []
    print('Performing logistic regression, please wait...')
    while percent <= 100:
        training_error_rate[percent] = 0
        test_error_rate[percent] = 0
        print(f'Percent of training set being used: {percent}')
        for i in range(0, folds):
            x_test_set = x_folds[i]
            y_test_set = y_folds[i]

            x_training_set = np.delete(x_folds, i, 0).reshape(training_size, 16)
            y_training_set = np.delete(y_folds, i, 0).reshape(training_size, )

            # create a subset of training data, based on our percentage
            x_training_set = x_training_set[0:int(training_size * 0.01 * percent)]
            y_training_set = y_training_set[0:int(training_size * 0.01 * percent)]
            model = learn_model((x_training_set, y_training_set))
            # get error rate of training data
            training_results = apply_model(model, x_training_set, threshold)
            training_error_rate[percent] += error_rate(y_training_set, training_results)
            # get error rate of test data
            test_results = apply_model(model, x_test_set, threshold)
            test_error_rate[percent] += error_rate(y_test_set, test_results)
        # we want an average from the results of the 5 folds
        training_error_rate[percent] = training_error_rate[percent] / folds
        test_error_rate[percent] = test_error_rate[percent] / folds
        percent += 20
    plt.figure(figsize=(10, 6))
    plt.plot(list(training_error_rate.keys()), list(training_error_rate.values()), color='b', label='Training')
    plt.plot(list(test_error_rate.keys()), list(test_error_rate.values()), color='r', label='Test')
    plt.xlabel('Training Data Percentage')
    plt.ylabel('Error Rate')
    plt.legend()
    plt.show()

    training_difference = abs(training_error_rate[100] - test_error_rate[100])
    high_variance_threshold = 0.05
    need_more_data = training_difference > high_variance_threshold
    if need_more_data:
        print(
            f'The difference between the training and test error rate at 100% was {training_difference} which is over our variance threshold {high_variance_threshold}. We need more data!')
    else:
        print(
            f'The difference between the training and test error rate at 100% was {training_difference} which is under our variance threshold {high_variance_threshold}. We do not need more data!')


def answer3():
    num_to_generate = 200
    threshold = 0.0
    thresholds = []
    training_error_rate = []
    test_error_rate = []
    data = generate_data(clean_data, int(num_to_generate / 2), "hills")
    x_training_set, y_training_set = zip(*data)
    x_training_set = np.array(x_training_set)
    y_training_set = np.array(y_training_set)
    test_data = generate_data(clean_data, int(num_to_generate / 2), "hills")
    x_test_set, y_test_set = zip(*test_data)
    x_test_set = np.array(x_test_set)
    y_test_set = np.array(y_test_set)

    for i in range(0, 11):
        thresholds.append(threshold)
        print(f'Performing logistic regression with threshold {threshold:.1f}...')
        model = learn_model((x_training_set, y_training_set))
        # get error rate of training data
        training_results = apply_model(model, x_training_set, threshold)
        training_error_rate.append(error_rate(y_training_set, training_results))
        # get error rate of test data
        test_results = apply_model(model, x_test_set, threshold)
        test_error_rate.append(error_rate(y_test_set, test_results))
        threshold += 0.1
    plt.figure(figsize=(10, 6))
    plt.plot(thresholds, training_error_rate, color='b', label='Training')
    plt.plot(thresholds, test_error_rate, color='r', label='Test')
    plt.xlabel('Logistic Regression Threshold')
    plt.ylabel('Error Rate')
    plt.legend()
    plt.show()

    min_error_index = np.argmin(test_error_rate)
    print(f'\nThe inflection point of the test curve is at index {min_error_index}.')
    print(f'Therefore, the best threshold would be {thresholds[min_error_index]:.1f}')


# normal_module8()
# answer1()
# answer2()
answer3()
