- Work iteratively.
- You may want to start by implementing the overall game logic, then the
Random player.
- Have the Random Player play against itself (or a Human) make sure the game
logic itself works.
- Then implement your heuristic and the minimax algorithm. Make sure that works.
- Then move on
to Alpha Beta pruning.


1. Work through the push.py file and put together a basic game with all the completion logic, printing out who wins, etc.
2. Play the game a bit with a friend or implement a Random Player. Look to see patterns in how you build up a win. This will inform your game heuristic. The heuristic should indicate how good the current board is for the current player relative to winning.
3. Implement minimax game tree search.
4. Implement alpha/beta pruning.

A few comments are in order because this often stumps students.

1. The minimax algorithm basically mimics "if I move X, they'll move Y, then I'll move A, then they'll move B...so I should move not move X" type of reasoning except it does it for every possible current move, counter move, etc.
2. You do not use your heuristic to evaluate the current board. You use it to evaluate hypothetical boards to pick a single move for the current board and player.
3. Those hypothetical boards are however far down the game tree you can search. That's the whole point of alpha/beta pruning...you should, theoretically, be able to search several ply further by pruning "dead ends"...moves that are obviously losers.

This should be enough to get you started down the right track.
