import sys
from copy import deepcopy
from random import randint, shuffle
from typing import List, Tuple

import push


###
# amill189 aka Adam Miller
###

def print_board(board: List[List[str]]):
    """ Helper function to output push board in an easy-to-read format.

    Args:
        board: The push board
    """
    print('\n' + '\n'.join(''.join(map(str, s1)) for s1 in board) + '\n')


def generate_random_move(player: str) -> Tuple[str, str, int]:
    """ Helper function to generate a random move

    Args:
        player: the player to make a random move

    Returns:
        A tuple containing the player, a random move type (one of top,
        bottom, left, or right), and a random index, from 1 through 4
    """
    move_types: List[str] = ['T', 'B', 'L', 'R']
    return player, move_types[randint(0, 3)], randint(1, 4)


def is_board_in_terminal_state(board: List[List[str]], board_states: List[List[List[str]]]) -> bool:
    """ Helper function that indicates if the board is in a terminal state.

        The board is in a terminal state if the board returns to a previous state or
        if a player has more "straights" than the other.

    Returns:
        Boolean value of true if the board is in a terminal state.
    """
    # get the number of straights on the board
    straights = push.straights(board)

    # return whether or not the board is in a terminal state
    return board_state_repeated(board, board_states) or straights.get('O') > straights.get('X') or straights.get(
        'X') > straights.get('O')


def board_state_repeated(board: List[List[str]], board_states: List[List[List[str]]]):
    """ Helper function to check if the current board is in the list of previous board states

    Returns:
        True if board is in the list of previous board states
    """
    if board not in board_states:
        return False
    else:
        return True


def board_score_for_player(board: List[List[str]], player: str) -> int:
    """ Helper method to return the heuristic value of the board state that is passed in.

    Args:
        board: The push board
        player: The player we are scoring

    Returns:
        A score representing the value of the board state
    """

    # there are 10 rows on the board, add them to rows
    rows: List[List[str]] = []
    rows.append(board[0])
    rows.append(board[1])
    rows.append(board[2])
    rows.append(board[3])

    #   4 vertical
    rows.append([row[0] for row in board])
    rows.append([row[1] for row in board])
    rows.append([row[2] for row in board])
    rows.append([row[3] for row in board])

    #   2 diagonal
    rows.append([board[i][i] for i in range(0, 4)])
    rows.append([board[i][3 - i] for i in range(0, 4)])

    #
    score = get_scores_for_rows_for_player(rows, player)
    return score


def get_scores_for_rows_for_player(rows: List[List[str]], player: str) -> int:
    """ Helper function to sum the scores of the 10 rows

    Args:
        rows: The ten rows that make up the board
        player: The player we are scoring

    Returns:
        The sum of the scores for each of the 10 rows

    """
    score: int = 0
    for row in rows:
        score += get_score_for_row(row, player)
    return score


def get_score_for_row(row: List[str], player: str):
    """ Helper function to return the score for a single row

    This function gets the number of our symbols as well as the
    number of our opponent's and calculates the score for the row.
    The score of the opponents is subtracted from our score.

    Args:
        row: A row from the board
        player: The player we are scoring

    Returns:
        The score for a row

    """
    opponent: str = other_player(player)
    # count of the opponent's symbols
    opponent_count: int = 0
    # count of our symbols
    player_count: int = 0
    for position in row:
        if position == player:
            player_count += 1
        elif position == opponent:
            opponent_count += 1
    return calc_score(player_count) - calc_score(opponent_count)


def calc_score(player_count: int):
    """ Helper function to calculate a score based on the number of symbols given

        If there are zero symbols, the score is zero.

        Otherwise, the score is relative to the number of symbols in a row. The score ramps up if 3 symbols are found
        and even more if 4 are found.
        0 symbols = 0
        1 symbol = 1
        2 symbols = 10
        3 symbols = 1000
        4 symbols = 1000000
    Args:
        player_count: The number of symbols in a row

    Returns:
        The score for a given number of symbols in a row
    """
    if player_count == 0:
        return 0
    elif player_count == 1:
        return 1
    elif player_count == 2:
        return 10
    elif player_count == 3:
        return 1000
    elif player_count == 4:
        return 1000000


def get_moves(player: str):
    """ Helper function to generate a list of the 16 possible moves.

    The moves will be randomized so that the same moves are not played
    again and again in the event of a tie.

    Args:
        player: The player for whom we are generating moves.

    Returns:
        A list of lists containing all of the possible moves that can be played.
    """
    moves = []
    for index in range(1, 5):
        moves.append([player, 'T', index])
        moves.append([player, 'B', index])
        moves.append([player, 'L', index])
        moves.append([player, 'R', index])

    # Randomize the order of the moves. This will prevent our bots from
    # doing the same thing over and over when there is a heuristic tie.
    shuffle(moves)
    return moves


def alpha_beta(board: List[List[str]], board_states: List[List[List[str]]], player: str, depth: int,
               is_max_player: bool, alpha: int, beta: int):
    """ This algorithm implements minimax with alpha beta pruning.

    It recursively calls itself until a depth of zero unreached, at which point it unfolds
    and returns a move with the highest value.

    Args:
        board: the board we are searching
        board_states: a history of board states to check for duplicate states
        player: the current player
        depth: the number of plies remaining to search
        is_max_player: true if we are searching for a max node
        alpha: the running highest value
        beta: the running lowest value

    Returns:
        The return value is a list with index 1 containing the position for the best move.
        Index 2 contains the index of the move to be played. Index 3 holds the score of
        the move based on the heuristic function.

    """
    # when we reach depth 0, return the score
    if depth == 0:
        # make sure that we are scoring the board based on the actual player
        if is_max_player:
            score = board_score_for_player(board, player)
        else:
            score = board_score_for_player(board, other_player(player))
        return ['', '', 0, score]
    if is_max_player:
        # find the move that maximizes value
        best_move = ['.', '', -1, -sys.maxsize]
    else:
        # find the move that minimizes value
        best_move = ['.', '', -1, sys.maxsize]

    # explore all of the possible moves
    for move in get_moves(player):
        # copy the board so we don't affect actual board
        search_board = deepcopy(board)
        search_board = push.move(search_board, move)
        if board_state_repeated(search_board, board_states):
            # don't pick a move that will result in a repeated board state
            continue
        # recurse, switching players and decreasing the depth by 1
        move_score = alpha_beta(search_board, board_states, other_player(player), depth - 1, not is_max_player,
                                alpha, beta)
        # if we're looking for a max node
        if is_max_player:
            if move_score[3] > best_move[3]:
                best_move = move
                best_move.append(move_score[3])
            # set alpha to the highest value that we've found so far
            alpha = max(alpha, best_move[3])

            # if alpha exceeds the value of beta, we can stop searching this branch
            if alpha >= beta:
                break
        # search for a min node
        else:
            if best_move[3] > move_score[3]:
                best_move = move
                best_move.append(move_score[3])
            # set beta to the lowest value that we've found so far
            beta = min(beta, best_move[3])
            # if alpha exceeds the value of beta, we can stop searching this branch
            if alpha >= beta:
                break
    return best_move


def minimax(board: List[List[str]], board_states: List[List[List[str]]], player: str, depth: int, is_max_player: bool):
    """ This algorithm implements minimax

    It recursively calls itself until a depth of zero unreached, at which point it unfolds
    and returns a move with the highest value.

    Args:
        board: the board we are searching
        board_states: a history of board states to check for duplicate states
        player: the current player
        depth: the number of plies remaining to search
        is_max_player: true if we are searching for a max node

    Returns:
        The return value is a list with index 1 containing the position for the best move.
        Index 2 contains the index of the move to be played. Index 3 holds the score of
        the move based on the heuristic function.

    """
    # when we reach depth 0, return the score
    if depth == 0:
        # make sure that we are scoring the board based on the actual player
        if is_max_player:
            score = board_score_for_player(board, player)
        else:
            score = board_score_for_player(board, other_player(player))
        return ['', '', 0, score]

    if is_max_player:
        # find the move that maximizes value
        best_move = ['.', 'Z', -1, -sys.maxsize]
    else:
        # find the move that minimizes value
        best_move = ['.', 'Z', -1, sys.maxsize]

    # explore all of the possible moves
    for move in get_moves(player):
        # copy the board so we don't affect actual board
        search_board = deepcopy(board)
        search_board = push.move(search_board, move)
        if board_state_repeated(search_board, board_states):
            # don't pick a move that will result in a repeated board state
            continue
        # recurse, switching players and decreasing the depth by 1
        move_score = minimax(search_board, board_states, other_player(player), depth - 1, not is_max_player)

        # look for a max node here
        if is_max_player:
            if move_score[3] > best_move[3]:
                best_move = move
                best_move.append(move_score[3])
        # look for a min node here
        else:
            if best_move[3] > move_score[3]:
                best_move = move
                best_move.append(move_score[3])
    return best_move


def other_player(player: str):
    """ Helper method that returns the other player.

    Args:
        player: a player

    Returns: the opposite of the provided player

    """
    if player == 'X':
        return 'O'
    else:
        return 'X'


def minimax_versus_random():
    """
    Runs minimax versus random bot 5 times.

    Minimax runs at a search depth of 3 ply.

    This takes roughly 10 seconds to run on my machine.
    """
    wins_for_minimax = 0
    wins_for_random = 0
    random_player: str = 'O'
    minimax_player: str = 'X'
    depth: int = 3
    board: List[List[str]]
    print("Minimax Player is searching", depth, "ply.")
    for number_of_runs in range(0, 5):

        board = push.create()
        board_states: List[List[List[str]]] = [board]

        # alternate starting player each game
        minimax_player = other_player(minimax_player)
        random_player = other_player(random_player)

        # X starts first
        current_turn: str = 'X'
        end_game: bool = False
        while not end_game:
            if minimax_player == current_turn:
                # play minimax turn
                minimax_move = minimax(board, board_states, minimax_player, depth, True)
                mini_move = (minimax_player, minimax_move[1], minimax_move[2])
                board = push.move(board, mini_move)
            else:
                # play random turn
                board = push.move(board, generate_random_move(random_player))
            current_turn = other_player(current_turn)
            end_game = is_board_in_terminal_state(board, board_states)
            # keep track of board states
            if not end_game:
                board_states.append(board)
        straights_result = push.straights(board)
        if board_state_repeated(board, board_states):
            # repeating the board state is a loss for that player
            if current_turn == minimax_player:
                wins_for_minimax += 1
            else:
                wins_for_random += 1
        elif straights_result.get(minimax_player) > straights_result.get(random_player):
            wins_for_minimax += 1
        elif straights_result.get(random_player) > straights_result.get(minimax_player):
            wins_for_random += 1
    print("\nFinal Game:")
    print_board(board)
    print("Minimax won %s games" % wins_for_minimax)
    print("Random won %s games" % wins_for_random)


def minimax_versus_alphabeta():
    """
    Runs minimax versus alpha-beta 5 times.

    Minimax runs at a depth of 2 ply.

    Alpha-beta pruning bot runs at a depth of 5 ply.

    This takes about two minutes to run on my machine.
    """
    wins_for_minimax = 0
    wins_for_alpha_beta = 0
    alpha_beta_player: str = 'O'
    minimax_player: str = 'X'
    minimax_depth: int = 2
    alpha_beta_depth: int = 5
    board: List[List[str]]
    print("Minimax Player is searching", minimax_depth, "ply.")
    print("Alpha Beta Player is searching", alpha_beta_depth, "ply.")
    for number_of_runs in range(0, 5):

        board = push.create()
        board_states: List[List[List[str]]] = [board]

        # alternate starting player each game
        minimax_player = other_player(minimax_player)
        alpha_beta_player = other_player(alpha_beta_player)

        # X starts first
        current_turn: str = 'X'
        end_game: bool = False
        while not end_game:
            if minimax_player == current_turn:
                # play minimax turn
                minimax_move = minimax(board, board_states, minimax_player, minimax_depth, True)
                mini_move = (minimax_player, minimax_move[1], minimax_move[2])
                board = push.move(board, mini_move)
            else:
                # alpha beta's turn
                ab_move = alpha_beta(board, board_states, alpha_beta_player, alpha_beta_depth, True, -sys.maxsize,
                                     sys.maxsize)
                alpha_move = (alpha_beta_player, ab_move[1], ab_move[2])
                board = push.move(board, alpha_move)
            current_turn = other_player(current_turn)
            end_game = is_board_in_terminal_state(board, board_states)
            # keep track of board states
            if not end_game:
                board_states.append(board)
        straights_result = push.straights(board)
        if board_state_repeated(board, board_states):
            # repeating the board state is a loss for that player
            if current_turn == minimax_player:
                wins_for_minimax += 1
            else:
                wins_for_alpha_beta += 1
        elif straights_result.get(minimax_player) > straights_result.get(alpha_beta_player):
            wins_for_minimax += 1
        elif straights_result.get(minimax_player) < straights_result.get(alpha_beta_player):
            wins_for_alpha_beta += 1
    print("\nFinal Game:")
    print_board(board)
    print("Minimax won %s games" % wins_for_minimax)
    print("Alpha Beta won %s games" % wins_for_alpha_beta)


if __name__ == "__main__":
    print("Random v. Minimax")
    minimax_versus_random()

    print("\nMinimax v. Alpha Beta")
    minimax_versus_alphabeta()
