import sys

###
# amill189 aka Adam Miller
###
from typing import Dict, List, Tuple

full_world = [
  ['.', '.', '.', '.', '.', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
  ['.', '.', '.', '.', '.', '.', '.', '*', '*', '*', '*', '*', '*', '*', '*', '*', '.', '.', 'x', 'x', 'x', 'x', 'x', 'x', 'x', '.', '.'],
  ['.', '.', '.', '.', 'x', 'x', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', 'x', 'x', 'x', '#', '#', '#', 'x', 'x', '#', '#'],
  ['.', '.', '.', '.', '#', 'x', 'x', 'x', '*', '*', '*', '*', '~', '~', '*', '*', '*', '*', '*', '.', '.', '#', '#', 'x', 'x', '#', '.'],
  ['.', '.', '.', '#', '#', 'x', 'x', '*', '*', '.', '.', '~', '~', '~', '~', '*', '*', '*', '.', '.', '.', '#', 'x', 'x', 'x', '#', '.'],
  ['.', '#', '#', '#', 'x', 'x', '#', '#', '.', '.', '.', '.', '~', '~', '~', '~', '~', '.', '.', '.', '.', '.', '#', 'x', '#', '.', '.'],
  ['.', '#', '#', 'x', 'x', '#', '#', '.', '.', '.', '.', '#', 'x', 'x', 'x', '~', '~', '~', '.', '.', '.', '.', '.', '#', '.', '.', '.'],
  ['.', '.', '#', '#', '#', '#', '#', '.', '.', '.', '.', '.', '.', '#', 'x', 'x', 'x', '~', '~', '~', '.', '.', '#', '#', '#', '.', '.'],
  ['.', '.', '.', '#', '#', '#', '.', '.', '.', '.', '.', '.', '#', '#', 'x', 'x', '.', '~', '~', '.', '.', '#', '#', '#', '.', '.', '.'],
  ['.', '.', '.', '~', '~', '~', '.', '.', '#', '#', '#', 'x', 'x', 'x', 'x', '.', '.', '.', '~', '.', '#', '#', '#', '.', '.', '.', '.'],
  ['.', '.', '~', '~', '~', '~', '~', '.', '#', '#', 'x', 'x', 'x', '#', '.', '.', '.', '.', '.', '#', 'x', 'x', 'x', '#', '.', '.', '.'],
  ['.', '~', '~', '~', '~', '~', '.', '.', '#', 'x', 'x', '#', '.', '.', '.', '.', '~', '~', '.', '.', '#', 'x', 'x', '#', '.', '.', '.'],
  ['~', '~', '~', '~', '~', '.', '.', '#', '#', 'x', 'x', '#', '.', '~', '~', '~', '~', '.', '.', '.', '#', 'x', '#', '.', '.', '.', '.'],
  ['.', '~', '~', '~', '~', '.', '.', '#', '*', '*', '#', '.', '.', '.', '.', '~', '~', '~', '~', '.', '.', '#', '.', '.', '.', '.', '.'],
  ['.', '.', '.', '.', 'x', '.', '.', '*', '*', '*', '*', '#', '#', '#', '#', '.', '~', '~', '~', '.', '.', '#', 'x', '#', '.', '.', '.'],
  ['.', '.', '.', 'x', 'x', 'x', '*', '*', '*', '*', '*', '*', 'x', 'x', 'x', '#', '#', '.', '~', '.', '#', 'x', 'x', '#', '.', '.', '.'],
  ['.', '.', 'x', 'x', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', 'x', 'x', 'x', '.', '.', 'x', 'x', 'x', '.', '.', '.', '.', '.'],
  ['.', '.', '.', 'x', 'x', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', 'x', 'x', 'x', 'x', '.', '.', '.', '.', '.', '.', '.'],
  ['.', '.', '.', 'x', 'x', 'x', '*', '*', '*', '*', '*', '*', '*', '*', '.', '.', '.', '#', '#', '.', '.', '.', '.', '.', '.', '.', '.'],
  ['.', '.', '.', '.', 'x', 'x', 'x', '*', '*', '*', '*', '*', '*', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '~', '~', '~', '~'],
  ['.', '.', '#', '#', '#', '#', 'x', 'x', '*', '*', '*', '*', '*', '.', 'x', '.', '.', '.', '.', '.', '~', '~', '~', '~', '~', '~', '~'],
  ['.', '.', '.', '.', '#', '#', '#', 'x', 'x', 'x', '*', '*', 'x', 'x', '.', '.', '.', '.', '.', '.', '~', '~', '~', '~', '~', '~', '~'],
  ['.', '.', '.', '.', '.', '.', '#', '#', '#', 'x', 'x', 'x', 'x', '.', '.', '.', '.', '#', '#', '.', '.', '~', '~', '~', '~', '~', '~'],
  ['.', '#', '#', '.', '.', '#', '#', '#', '#', '#', '.', '.', '.', '.', '.', '#', '#', 'x', 'x', '#', '#', '.', '~', '~', '~', '~', '~'],
  ['#', 'x', '#', '#', '#', '#', '.', '.', '.', '.', '.', 'x', 'x', 'x', '#', '#', 'x', 'x', '.', 'x', 'x', '#', '#', '~', '~', '~', '~'],
  ['#', 'x', 'x', 'x', '#', '.', '.', '.', '.', '.', '#', '#', 'x', 'x', 'x', 'x', '#', '#', '#', '#', 'x', 'x', 'x', '~', '~', '~', '~'],
  ['#', '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#', '#', '#', '#', '#', '.', '.', '.', '.', '#', '#', '#', '.', '.', '.']]

test_world = [
  ['.', '*', '*', '*', '*', '*', '*'],
  ['.', '*', '*', '*', '*', '*', '*'],
  ['.', '*', '*', '*', '*', '*', '*'],
  ['.', '.', '.', '.', '.', '.', '.'],
  ['*', '*', '*', '*', '*', '*', '.'],
  ['*', '*', '*', '*', '*', '*', '.'],
  ['*', '*', '*', '*', '*', '*', '.'],
]

cardinal_moves = [(0, -1), (1, 0), (0, 1), (-1, 0)]
costs = {'.': 1, '*': 3, '#': 5, '~': 7}


def heuristic(current_node: Tuple[int, int], goal: Tuple[int, int]) -> int:
    """ Calculates distance heuristic of two points.

        Manhattan distance is the chosen heuristic because it is guaranteed
        never to exceed the actual distance between two points in our world.

        Euclidean distance will also always underestimate the actual value of the
        distance because our algorithm only supports cardinal movement. However,
        Manhattan distance was chosen over Euclidean distance since Manhattan
        distance will always be greater than or equal to Euclidean distance while
        still being guaranteed not to exceed the actual distance.

        Manhattan distance of two points p1(x1, y2) and p2(x2, y2) is:
        |x1 - x2| + |y1 - yx|

    Args:
        current_node: A tuple representing the an x,y coordinate of some point
                    from which we want to calculate our heuristic function.
        goal: A tuple representing the x,y coordinate of the goal.

    Returns:
        An integer value of the Manhattan distance between the two parameters
        provided to this function.
    """
    # change the parameters as needed and return the value of h(node)
    # you think is best and admissible

    # the Manhattan distance between the current node and the goal node
    return abs(current_node[0] - goal[0]) + abs(current_node[1] - goal[1])


def a_star_search(world: List[List[str]], start: Tuple[int, int], goal: Tuple[int, int],
                  costs: Dict[str, int], moves: List[Tuple[int, int]], debug: bool) -> List[Tuple[int, int]]:
    """ Performs A* Search to return moves of lowest cost path.

    This method extends the basic graph search pseudo-code defined in module 1:
    # 1	place initial state on frontier
    # 2	initialize explored list
    # 3	while frontier is not empty
    # 4	  current-state := minimum cost state on frontier
    # 5   remove current-state  from frontier
    # 6	  return path( current-state) if is-terminal( current-state)
    # 7	  children := successors( current-state)
    # 8	  for each child in children
    # 9	    add child to frontier if not on explored or frontier
    # 10  add current-state to explored
    # 11	return nil

    The frontier for the search is a dict with a Tuple containing an x,y coordinate
    as the key and the value of f(n) = g(n) + h(n) where g(n) is the cost of the
    path from the start until n, and h(n) is the estimated cost from n to the goal,
    returned by the heuristic method above.

    The current-state's children are generated and f(n) = g(n) + h(n) is calculated
    for each child if they are not explored or in the frontier. An important note
    is that in this loop, g(n) values of children are updated if a lower g(n) is
    found than previously and conversely, higher values are ignored.

    Args:
        world: The world we are searching.
        start: A tuple representing the x,y coordinates of our "starting" node.
        goal: A tuple representing the x,y coordinates of our "goal" node.
        costs: A dict that defines the various "costs" associated with the different
            types of terrain in our world.
        moves: A list that defines the different types of moves that are valid from
            one point to the next.
        debug: Flag that enables debug message output.

    Returns:
        A list of tuples that represent the moves necessary to move from start to
        goal with the lowest cost possible. An empty list is returned if no path
        exists from the start to the goal.

    """
    # Check to ensure that we aren't starting on the mountains!
    if world[start[1]][start[0]] == 'x':
        return []

    # frontier Dict with key=Tuple containing x,y coordinates and value=f(n) of the key
    frontier: Dict[Tuple[int, int], int] = {start: estimated_node_cost(start, goal, world)}

    # explored node list containing Tuple of x,y coordinates of the explored node
    explored: List[Tuple[int, int]] = []

    # Dict whose key is a child node and the value is that child's parent. This can
    # be used as a sort of "linked list" to find our path once the goal is reached.
    previous_states: Dict[Tuple[int, int], Tuple[int, int]] = {}

    # Dict that keeps track of our g(n) values.
    cost_from_start: Dict[Tuple[int, int], int] = {start: 0}

    # Loop through frontier, examining the lowest cost nodes until the frontier is
    # empty, or until we reach our goal.
    while frontier:

        # Sort the frontier dict and set current state to the node with the lowest cost
        current_state: Tuple[int, int] = sorted(frontier.items(), key=lambda x: x[1])[0][0]

        # Remove the current-state from the frontier
        frontier.pop(current_state)

        # If we've reached the goal, pass references of our state paths so "moves"
        # can be generated.
        if current_state == goal:
            return convert_path_to_moves(previous_states, current_state)

        # Loop through the children of the current_state.
        for child in find_successors(current_state, world):

            # Cost of the path from the start node to child node
            g_child: int = cost_from_start[current_state] + get_node_cost_from_world(child, world)

            # Add child and its f(n) to the frontier, if it is not yet there
            if child not in frontier and child not in explored:
                frontier[child] = g_child + heuristic(child, goal)

            # Ignore paths to this child with greater costs than we have already seen
            elif g_child >= cost_from_start[child]:
                continue

            # Link the current state as the previous state of this child
            previous_states[child] = current_state

            # Store the cost from the start of this child, aka G(n)
            cost_from_start[child] = g_child

        # Add the current state to the explored nodes
        explored.append(current_state)

    # If the goal was not found, return an empty list
    return []


def find_successors(current_state: Tuple[int, int], world: List[List[str]]) -> List[Tuple[int, int]]:
    """ Returns a list of children reachable from a given state in a given world.

    Args:
        current_state: The "parent" node of the children that we are searching for.
        world: The world we are searching.

    Returns:
        A List containing the nodes that are reachable from current_state. The
        children are reachable from the current_state by moving in the directions
        specified  in the `cardinal_moves` list. If no children are found, an
        empty list is returned.

    """
    children: List[Tuple[int, int]] = []

    # Attempt to find a child by using each transition function specified in cardinal_moves.
    for node in cardinal_moves:

        # Create x and y coordinates of the potential child
        x_pos: int = current_state[0] + node[0]
        y_pos: int = current_state[1] + node[1]

        # Check if the coordinates generated by the transition function are valid
        if 0 <= x_pos < len(world[0]) and 0 <= y_pos < len(world[1]):
            child_node: 'str' = world[y_pos][x_pos]

            # Ignore mountains as they are impassible
            if child_node != 'x':
                child: Tuple[int, int] = x_pos, y_pos
                children.append(child)

    return children


def estimated_node_cost(node: Tuple[int, int], goal: Tuple[int, int], world: List[List[str]]) -> int:
    """ Helper function to return a node's cost plus its estimated cost.

    Args:
        node: A given node in the world.
        goal: The goal node in the world.
        world: The world we are searching.

    Returns:
        The sum of h(n) + n

    """

    return heuristic(node, goal) + get_node_cost_from_world(node, world)


def get_node_cost_from_world(node: Tuple[int, int], world: List[List[str]]) -> int:
    """ Helper function to return the cost of a node in the world.

    Args:
        node: A given node in the world.
        world: The world we are searching.

    Returns:
        Cost of a given node in the world.

    """

    return costs[world[node[1]][node[0]]]


def convert_path_to_moves(previous_states: Dict[Tuple[int, int], Tuple[int, int]],
                          current_state: Tuple[int, int]) -> List[Tuple[int, int]]:
    """ Converts the path containing coordinates from start to goal into "moves"

    Args:
        previous_states: Dict whose key is a child node and the value is that child's parent.
        current_state: The "parent" node of the children that we are searching for.

    Returns:
        List containing Tuples of the x,y coordinates of "moves" to get from start to finish.

    """

    # List of x,y coordinates that will hold the path from start to goal
    path: List[Tuple[int, int]] = [current_state]

    # List of x,y coordinates that will hold the "moves" from start to goal
    moves: List[Tuple[int, int]] = []

    # Append all of the previous states to create the a path of x,y points from goal to start
    while current_state in previous_states:
        current_state = previous_states[current_state]
        path.append(current_state)

    # Reverse path so that it holds the path of x,y points from start to goal
    path.reverse()

    # Iterate through all of the nodes in the path, tracking two at a time, so that we can calculate a list of moves
    x: int = 0
    while x < len(path) - 1:
        item1: Tuple[int, int] = path[x]
        item2: Tuple[int, int] = path[x + 1]

        # The "move" is a Tuple containing the difference between the x coordinates
        # and the difference between the y coordinates
        move: Tuple[int, int] = item2[0] - item1[0], item2[1] - item1[1]
        moves.append(move)
        x += 1

    return moves


def pretty_print_solution(world: List[List[str]], path: List[Tuple[int, int]], start: Tuple[int, int], debug: bool):
    """ Prints grid of the world displaying the direction of moves to get from start to goal with minimum cost.

    Args:
        world: The world we are searching
        path: List containing Tuples of the x,y coordinates of "moves" to get from start to finish.
        start: A tuple representing the x,y coordinates of our "starting" node.
        debug: Flag that enables debug message output.
    """

    pretty_path: List[List[str]] = world

    # Dict containing "moves" as the key and the value of the "arrow" that it represents
    arrows: Dict[Tuple[int, int], str] = {(0, -1): '^', (1, 0): '>', (0, 1): 'V', (-1, 0): '<'}

    # Variable to track the running cost of the moves
    cost: int = get_node_cost_from_world(start, world)

    # Index node to navigate the world as we iterate through the "moves".
    # current_node[0] is the x-coordinate and current_node[1] is the y-coordinate.
    current_node: List[int] = list(start)

    for move in path:

        # Set the arrow to according to the value of the current "move"
        arrow = arrows.get(move)

        # Keep running cost of moves
        cost += get_node_cost_from_world((current_node[0], current_node[1]), world)

        # Put the arrow of this move in our "pretty-path" world
        pretty_path[current_node[1]][current_node[0]] = arrow

        # Increment our world navigator node
        current_node[0] += move[0]
        current_node[1] += move[1]

    # Set the goal node to 'G'
    pretty_path[current_node[1]][current_node[0]] = 'G'

    # Print the world with arrows indicating the path of least cost.
    print_world(pretty_path)

    if debug:
        print("Total cost of the path: ", cost)


def print_world(world: List[List[str]]):
    """ Helper method to output worlds in an easy-to-read format.

    Args:
        world: The world we are searching.
    """
    print('\n' + '\n'.join(' '.join(map(str, s1)) for s1 in world) + '\n')


if __name__ == "__main__":
    # use this debug flag if necessary; otherwise, the program should print out only
    # what is requested.
    debug = len(sys.argv) > 1 and sys.argv[1].lower() == 'debug'

    if debug:
        print_world(test_world)
    print("A* solution for test world")
    test_path = a_star_search(test_world, (0, 0), (6, 6), costs, cardinal_moves, debug)
    if test_path:
        print(test_path)
        pretty_print_solution(test_world, test_path, (0, 0), debug)
    else:
        print("A solution for was not found for test world.")

    if debug:
        print_world(full_world)
    print("A* solution for full world")
    full_path = a_star_search(full_world, (0, 0), (26, 26), costs, cardinal_moves, debug)
    if full_path:
        print(full_path)
        pretty_print_solution(full_world, full_path, (0, 0), debug)
    else:
        print("A solution for was not found for full world.")
